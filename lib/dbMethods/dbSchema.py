import sqlite3
from sqlite3 import Error
import sys
sys.path.append('lib')

from sqlalchemy import Column, Date, Integer, String,Float, Boolean, VARCHAR
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine, MetaData, Table, inspect,ForeignKey,Index
from sqlalchemy.sql import select
from sqlalchemy.orm import sessionmaker
from sqlalchemy import func
from sqlalchemy import Unicode, UnicodeText
from sqlalchemy.orm import deferred, relationship
from sqlalchemy.ext.hybrid import hybrid_property
from unidecode import unidecode

from matplotlib import pyplot as plt
from scipy.stats import mode
import pandas as pd
from unidecode import unidecode
from pprint import pprint
import uuid

patentBase = declarative_base()
cascade = "all, delete, delete-orphan"

## Edit this function if you want to change the db table, simple add a class like the keywords below

def _create_data_base(dir):
    engine = create_engine(dir, echo=False)
    patentBase.metadata.create_all(engine)
    return engine

class Patent(patentBase):
    __tablename__ = "patent"
    patent_id = Column(Unicode(20), primary_key=True)
    keywords = relationship("keywords", backref="patent", cascade=cascade)
    gramWords = relationship("gramWords", backref="patent", cascade=cascade)

class keywords(patentBase):
    __tablename__ = "keywords"
    # uuid= deferred(Column(UnicodeText,primary_key = True))
    patent_id = Column(Unicode(20),primary_key = True)
    keyword = deferred(Column(UnicodeText,primary_key = False))


class gramWords(patentBase):
    __tablename__ = "grammar"
    # uuid = deferred(Column(UnicodeText,primary_key = False))
    patent_id = Column(Unicode(20),primary_key = True)
    word = deferred(Column(UnicodeText,primary_key = False))
    # sequence = Column(Integer,primary_key=True, autoincrement=True)
