import sqlite3
from sqlite3 import Error
import sys
sys.path.append('lib')
import getpass

from sqlalchemy import create_engine, MetaData, Table, inspect
from sqlalchemy.sql import select
from sqlalchemy.orm import sessionmaker, defer
from matplotlib import pyplot as plt
from scipy.stats import mode
import pandas as pd
from unidecode import unidecode
from lib.alchemy.schema import GrantBase, ApplicationBase
from lib.alchemy import session_generator
import dbSchema
from pprint import pprint
from dbSchema import patentBase

class processedPatent():
    def __init__(self,year=""):
        self.year = year
        dir = 'sqlite:////home/'+getpass.getuser()+'/patents/features/grant_'+year+'.db'
        engine = dbSchema._create_data_base(dir)
        # engine = create_engine('sqlite:///../../processedPatent.db', echo=True)
        Session = sessionmaker()
        Session.configure(bind=engine)
        self.session = Session()
        print "finised reading",year

    # def getAllTableNames(self):
    #     ''' Function to get all the SQL table names
    #         input()
    #         output:
    #             tableNames : array of table names as strings
    #     '''
    #     tables = GrantBase.metadata.tables
    #     rawTables = tables.keys()
    #     return rawTables


    def getAllGrammar(self):
        ''' Function to get all patents
            input: None
            output:
                allPatents : patentObject
        '''
        return self.session.query(dbSchema.gramWords).all()

    def getByPatentID(self, Patent_ID):
        ''' Function to get all patents
            input: None
            output:
                allPatents : patentObject
        '''
        return self.session.query(dbSchema.Patent).filter(dbSchema.Patent.patent_id == Patent_ID)
