import sqlite3
from sqlite3 import Error
import sys
import dbSchema
import getpass
sys.path.append('lib')

from sqlalchemy import Column, Date, Integer, String,Float, Boolean, VARCHAR
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine, MetaData, Table, inspect,ForeignKey,Index
from sqlalchemy.sql import select
from sqlalchemy.orm import sessionmaker
from sqlalchemy import func
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy import Unicode, UnicodeText
from sqlalchemy.orm import deferred, relationship
from sqlalchemy.ext.hybrid import hybrid_property
from unidecode import unidecode
import uuid
from sqlalchemy import and_
from sqlalchemy import *
from sqlalchemy.orm import *
from matplotlib import pyplot as plt
from scipy.stats import mode
import pandas as pd
from unidecode import unidecode
from pprint import pprint



class DBWriteMethods():
    def __init__(self,year=""):
        self.year = year
        dir = 'sqlite:////home/'+getpass.getuser()+'/patents/features/grant_'+year+'.db'
        engine = dbSchema._create_data_base(dir)
        # engine = create_engine('sqlite:///../../processedPatent.db', echo=True)
        Session = sessionmaker()
        Session.configure(bind=engine)
        self.session = Session()

    def addpatent(self,patent):
        '''
        inputs - list of keywords/patent_id dicts of ({'patent_id' : 'val', 'words':['val1','val2','val3']})
        outputs - null
        '''
        try:
            row = dbSchema.Patent(patent_id=patent)
            self.session.add(row)
            # self.session.commit()
        except SQLAlchemyError as e:
            print e
        # finally:
        #     self.session.close()

    def addGrammarlist(self,listOfDicts):
        '''
        inputs - list of keywords/patent_id dicts of ({'patent_id' : 'val', 'words':['val1','val2','val3']})
        outputs - null
        '''
        try:
            row = dbSchema.gramWords(patent_id=listOfDicts['patent_id'], word=listOfDicts['words'])
            self.session.add(row)
            # self.session.commit()
        except SQLAlchemyError as e:
            print e
        # finally:
        #     self.session.close()

    def addKeywordslist(self,listOfDicts):
        '''
        inputs - list of keywords/patent_id dicts of ({'patent_id' : 'val', 'words':['val1','val2','val3']})
        outputs - null
        '''
        try:
            row = dbSchema.keywords(patent_id=listOfDicts['patent_id'], keyword=listOfDicts['words'])
            self.session.add(row)
            self.session.commit()
        except SQLAlchemyError as e:
            print e
        # finally:
        #     self.session.close()

    # def _commit(self):
    #     self.session.commit()
        # self.session.close()

    # def addKeywords(self, listOfkeywords):
    #     '''
    #     inputs - list of keywords/patent_id dicts of ({'patent_id' : 'val', 'words':['val1','val2','val3']})
    #     outputs - null
    #     '''
    #     try:
    #         # add row to database
    #         for i,keyword in enumerate(listOfkeywords['words']):
    #             row = dbSchema.keywords(uuid=uuid.uuid4().hex, patent_id=listOfkeywords['patent_id'], keyword=keyword, sequence= i )
    #             _exists = self.session.query(exists().where( and_(dbSchema.keywords.patent_id==listOfkeywords['patent_id'], dbSchema.keywords.keyword == keyword) )).scalar()
    #             if not _exists:
    #                  self.session.add(row)
    #             else:
    #                 print('Already Exists')
    #
    #         self.session.commit()
    #     except SQLAlchemyError as e:
    #         print e
    #     finally:
    #         self.session.close()
    #
    # def updateKeywords(self, listOfkeywords):
    #     '''
    #     inputs - list of keywords/patent_id dicts of ({'patent_id' : 'val', 'words':['val1','val2','val3']})
    #     outputs - null
    #     '''
    #     try:
    #         # DELETE all entried for patent_id
    #         self.session.query(dbSchema.keywords.patent_id).filter(dbSchema.keywords.patent_id==listOfkeywords['patent_id']).delete(synchronize_session='fetch')
    #         self.session.commit()
    #         # add row to database
    #         for i,keyword in enumerate(listOfkeywords['words']):
    #             row = dbSchema.keywords(uuid=uuid.uuid4().hex, patent_id=listOfkeywords['patent_id'], keyword=keyword, sequence= i )
    #             self.session.add(row)
    #         self.session.commit()
    #     except SQLAlchemyError as e:
    #         print('ERROR')
    #         print e
    #     finally:
    #         self.session.close()
    #
    #
    #
    # def addGrammarWords(self,listOfDicts):
    #     '''
    #     inputs - list of keywords/patent_id dicts of ({'patent_id' : 'val', 'words':['val1','val2','val3']})
    #     outputs - null
    #     '''
    #     try:
    #         # add row to database
    #         for i,word in enumerate(listOfDicts['words']):
    #             row = dbSchema.gramWords(uuid=uuid.uuid4().hex, patent_id=listOfDicts['patent_id'], word=word, sequence= i)
    #             _exists = self.session.query(exists().where( and_(dbSchema.gramWords.patent_id==listOfDicts['patent_id'], dbSchema.gramWords.word == word) )).scalar()
    #             if not _exists:
    #                 self.session.add(row)
    #             else:
    #                 print('Already Exists',word)
    #         #         # print(listOfDicts['words'])
    #         self.session.commit()
    #     except SQLAlchemyError as e:
    #         print e
    #     finally:
    #         self.session.close()



    # def updateGrammarWords(self, listOfDicts):
    #     '''
    #     inputs - list of keywords/patent_id dicts of ({'patent_id' : 'val', 'words':['val1','val2','val3']})
    #     outputs - null
    #     '''
    #     try:
    #         # DELETE all entried for patent_id
    #         self.session.query(dbSchema.gramWords.patent_id).filter(dbSchema.gramWords.patent_id==listOfDicts['patent_id']).delete(synchronize_session='fetch')
    #         self.session.commit()
    #         # add row to database
    #         for i,word in enumerate(listOfDicts['words']):
    #             row = dbSchema.gramWords(uuid=uuid.uuid4().hex, patent_id=listOfDicts['patent_id'], word=word, sequence= i)
    #             self.session.add(row)
    #         self.session.commit()
    #     except SQLAlchemyError as e:
    #         print('ERROR')
    #         print e
    #     finally:
    #         self.session.close()



# EXAMPLE
# 1) Run python2.7 ../../dbSchema
# 2) then access DBWriteMethods

# me = DBWriteMethods()

# keywordDict = {'patent_id' : 'val', 'words':['val1','val2','val3']}
# me.addKeywords(keywordDict)
# me.updateKeywords(keywordDict)

# gramDict = {'patent_id' : 'val', 'words':['val1','val2','val3']}
# me.addGrammarWords(gramDict)
# me.updateGrammarWords(gramDict)
