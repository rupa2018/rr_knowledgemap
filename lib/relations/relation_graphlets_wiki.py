import sys,glob,os
import getpass
import logging
import datetime
import cPickle
import spacy
import numpy as np
import string
import gensim
from gensim.utils import lemmatize
from extract_graphlet_v5 import extractGraphlets

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

class relation_graphlets():
    """docstring for spacy_chunks."""
    def __init__(self, logger):
        self.logger = logger
        self.user = getpass.getuser()
        self.project = "all-wiki"
        self.nlp = spacy.load('en_core_web_sm')
        self.EG = extractGraphlets()

        if os.path.isdir("/media/"+self.user+"/Data4"):
            self.data_dir = "/media/"+self.user+"/Data4/ROSA/analysis/"+self.project+"/"
            self.save_dir = "/media/"+self.user+"/Data4/ROSA/db/"
        elif os.path.isdir("/home/"+self.user+"/Data4"):
            self.data_dir = "/home/"+self.user+"/Data4/ROSA/analysis/2018-08-06-11_45_28-(gas turbine)/"
            self.save_dir = "/home/"+self.user+"/Data4/ROSA/db/all-claims/"
        self.counter = 0

        self.logger.info("learning is-a relations ready..")

    def _process_all(self):
        if not os.path.isdir(self.save_dir):
            os.mkdir(self.save_dir)
        self.save_dir += "graphlets/"
        if not os.path.isdir(self.save_dir):
            os.mkdir(self.save_dir)

        self.folders = sorted([x[0] for x in os.walk(self.data_dir)])

        for folder in self.folders:
            self.name = folder.split("/")[-1]
            if not os.path.isdir(self.save_dir+self.name):
                os.mkdir(self.save_dir+self.name)

            for file_ in sorted(glob.glob(folder+"/*.p")):
                self.file_name = file_.split("/")[-1].replace(".p",".txt")
                if os.path.isfile(self.save_dir+self.name+"/"+self.file_name):
                    continue
                self.logger.info("processing "+self.name+"-"+self.file_name)
                self.wiki_page = cPickle.load( open(file_, "r") )
                self._extract_graphlets()

    def _extract_graphlets(self):

        # self.F_is = open(self.save_dir+self.name+"/"+self.file_name,"w")
        for id_ in self.wiki_page:
            for count,sentence in enumerate(self.wiki_page[id_]["text"]):
                print self.wiki_page[id_]["text"][count]
                relations = self.EG._process(sentence.decode('utf8'), self.nlp)
                for r in relations:
                    if "includ" in r[1]:
                        print bcolors.FAIL + r[0] + bcolors.ENDC, "->",
                        print bcolors.OKBLUE + r[1] + bcolors.ENDC, "->",
                        print bcolors.WARNING + r[2] + bcolors.ENDC, "->",
                        print self.wiki_page[id_]["title"],count,self.wiki_page[id_]["url"]
                print "------------------"
                # sys.exit(1)









        ##
