import sys,glob,os
import getpass
import logging
import datetime
import cPickle
import spacy
import numpy as np
import string
import gensim
from gensim.utils import lemmatize
from extract_graphlet_v5 import extractGraphlets
from lib.dbMethods import dbMethods
from multiprocessing import Pool

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

############### parallel stuff
def _is_ascii_1( s):
    printable = set(string.printable)
    return filter(lambda x: x in printable, s)

def _read_descriptions_1(file_):
    the_data = dbMethods.patentMethods(file_)
    data = {}

    #########################################
    print "reading patents"
    patents = the_data.getAllPatents()
    for p in patents:
        patent_id = _is_ascii_1(p.id)
        title = _is_ascii_1(p.title)
        # abstract = _is_ascii_1(p.abstract)

        # if patent_id in df_reduced_patent_claims_:
        if patent_id not in data:
                data[patent_id] = {}
                data[patent_id]["title"] = patent_id + "--==--" + title
                data[patent_id]["organization"] = ""
                data[patent_id]["inventor"] = ""
                data[patent_id]["text"] = ""

    #########################################
    print "reading orgs"
    orgs = the_data.getAllOrgs()
    for o in orgs:
        patent_id = _is_ascii_1(o.patent_id)
        # if patent_id in df_reduced_patent_claims_:
        org = _is_ascii_1(o.organization)
        name1 = _is_ascii_1(o.name_first)
        name2 = _is_ascii_1(o.name_last)
        if name1 != "":
            inventor =  name1+";"+name2
            if data[patent_id]["inventor"] == "":
                data[patent_id]["inventor"] = inventor
            else:
                data[patent_id]["inventor"] += "---" + inventor
        data[patent_id]["organization"] += org

    #########################################
    print "reading descriptions"
    descriptions = the_data.getAllDescriptions()
    for d in descriptions:
        patent_id = _is_ascii_1(d.patent_id)
        # if patent_id in df_reduced_patent_claims_:
        # print len(d.text.split(";")), d.text.split(";")[0]
        descr_text = _is_ascii_1(d.text)

        if data[patent_id]["text"] == "":
            data[patent_id]["text"] = descr_text
        else:
            data[patent_id]["text"] += " "+descr_text
    return data

def _extract_graphlets_1(data, save_dir, name, file_name, nlp, EG, date):
    print "extracting graphlets for "+file_name
    for id_ in data:
        if "gas turbine" in data[id_]["text"]:
            F_is = open(save_dir+name+"/"+file_name+"__"+id_+".txt","w")

            doc = nlp(data[id_]["text"].decode('utf8'))

            for count, sent in enumerate(doc.sents):
                # print sent.string.strip()
                F_is.write("SENTENCE="+sent.string.strip()+"\n")
                relations = EG._process(sent.string.strip(), nlp)
                for r in relations:
                    txt =  bcolors.FAIL + r[0] + bcolors.ENDC + " -->> "
                    txt += bcolors.OKBLUE + r[1] + bcolors.ENDC + " -->> "
                    txt += bcolors.WARNING + r[2] + bcolors.ENDC + " -->> "

                    txt2 =  r[0] + " -->> "
                    txt2 += r[1] + " -->> "
                    txt2 += r[2] + " -->> "

                    meta = "patent_id="+id_+"--==--"
                    meta += "sentence="+str(count)+"--==--"
                    meta += "organization="+data[id_]["organization"]+"--==--"
                    meta += "date="+date
                    # print txt + meta
                    F_is.write("RELATION="+txt2+meta+"\n")
                # print "------------------"
                F_is.write("------------------"+"\n")
    F_is = open(save_dir+name+"/finished.data","w")

nlp = spacy.load('en_core_web_sm')

def _process_1(data):
    file_, save_dir = data
    EG = extractGraphlets()
    name = file_.split("/")[-1].split(".")[0]
    if not os.path.isdir(save_dir+name):
        os.mkdir(save_dir+name)

    file_name = name
    if os.path.isfile(save_dir+name+"/finished.data"):
        print "I have processed "+save_dir+name+"/"+file_name
    else:
        print "processing "+name+"-"+file_name

        date = "20"+file_name.split(".")[0].split("ipg")[1]
        data = _read_descriptions_1(file_)
        _extract_graphlets_1(data, save_dir, name, file_name, nlp, EG, date)


############### class stuff
class relation_graphlets():
    """docstring for spacy_chunks."""
    def __init__(self, logger, year = "all"):
        self.logger = logger
        self.year = year
        self.user = getpass.getuser()
        self.project = "all-claims"
        if os.path.isdir("/media/"+self.user+"/Data4"):
            self.data_dir = "/media/"+self.user+"/Data1/patents/extracted/"
            self.save_dir = "/media/"+self.user+"/Data4/ROSA/db/"
        elif os.path.isdir("/home/"+self.user+"/Data4"):
            self.data_dir = "/home/"+self.user+"/Data4/ROSA/analysis/2018-08-06-11_45_28-(gas turbine)/"
            self.save_dir = "/home/"+self.user+"/Data4/ROSA/db/all-descriptions/"
        self.counter = 0
        self.nlp = spacy.load('en_core_web_sm')
        self.EG = extractGraphlets()
        self.p = Pool(20)

        self.logger.info("learning is-a relations ready..")

    def _process_all(self):
        if not os.path.isdir(self.save_dir):
            os.mkdir(self.save_dir)
        self.save_dir += "graphlets/"
        if not os.path.isdir(self.save_dir):
            os.mkdir(self.save_dir)

        self.files = sorted(glob.glob(self.data_dir+"*.db"))

        ############## PARALLEL
        # data = [[k, self.save_dir] for k in self.files]
        # self.p.map(_process_1, data)

        ############# SERIAL
        for file_ in self.files:
            self.name = file_.split("/")[-1].split(".")[0]
            self.file_name = self.name
            self.date = "20"+self.file_name.split(".")[0].split("ipg")[1]

            if self.year == "all" or self.year in self.date[:4]:
                if not os.path.isdir(self.save_dir+self.name):
                    os.mkdir(self.save_dir+self.name)

                if os.path.isfile(self.save_dir+self.name+"/finished.data"):
                    print "I have processed "+self.save_dir+self.name+"/"+self.file_name
                    continue
                self.logger.info("processing "+self.name+"-"+self.file_name)

                self._read_descriptions(file_)
                self._extract_graphlets()

    def _is_ascii(self, s):
        printable = set(string.printable)
        return filter(lambda x: x in printable, s)

    def _read_descriptions(self, file_):
        data = dbMethods.patentMethods(file_)
        self.data = {}

        # df_reduced_patent_claims_ = P._read_csv(reduced_patent_claims_directory + filename_claims[i])
        # self.data["0"] = {}
        # self.data["0"]["title"] = "test"
        # self.data["0"]["organization"] = "test"
        # self.data["0"]["inventor"] = "test"
        # self.data["0"]["text"] = open("/home/mo//test.txt","r").read()

        #########################################
        print "reading patents"
        patents = data.getAllPatents()
        for p in patents:
            patent_id = self._is_ascii(p.id)
            title = self._is_ascii(p.title)
            # abstract = self._is_ascii(p.abstract)

            # if patent_id in df_reduced_patent_claims_:
            if patent_id not in self.data:
                    self.data[patent_id] = {}
                    self.data[patent_id]["title"] = patent_id + "--==--" + title
                    self.data[patent_id]["organization"] = ""
                    self.data[patent_id]["inventor"] = ""
                    self.data[patent_id]["text"] = ""

        #########################################
        print "reading orgs"
        orgs = data.getAllOrgs()
        for o in orgs:
            patent_id = self._is_ascii(o.patent_id)
            # if patent_id in df_reduced_patent_claims_:
            org = self._is_ascii(o.organization)
            name1 = self._is_ascii(o.name_first)
            name2 = self._is_ascii(o.name_last)
            if name1 != "":
                inventor =  name1+";"+name2
                if self.data[patent_id]["inventor"] == "":
                    self.data[patent_id]["inventor"] = inventor
                else:
                    self.data[patent_id]["inventor"] += "---" + inventor
            self.data[patent_id]["organization"] += org

        #########################################
        print "reading descriptions"
        descriptions = data.getAllDescriptions()
        for d in descriptions:
            patent_id = self._is_ascii(d.patent_id)
            # if patent_id in df_reduced_patent_claims_:
            # print len(d.text.split(";")), d.text.split(";")[0]
            descr_text = self._is_ascii(d.text)

            if self.data[patent_id]["text"] == "":
                self.data[patent_id]["text"] = descr_text
            else:
                self.data[patent_id]["text"] += " "+descr_text

    def _extract_graphlets(self):
        for id_ in self.data:
            if "gas turbine" in self.data[id_]["text"]:
                self.F_is = open(self.save_dir+self.name+"/"+self.file_name+"__"+id_+".txt","w")
                if len(self.data[id_]["text"])<100000:
                    doc = self.nlp(self.data[id_]["text"].decode('utf8'))

                    for count, sent in enumerate(doc.sents):
                        print sent.string.strip()
                        self.F_is.write("SENTENCE="+sent.string.strip()+"\n")
                        relations = self.EG._process(sent.string.strip(), self.nlp)
                        for r in relations:
                            txt =  bcolors.FAIL + r[0] + bcolors.ENDC + " -->> "
                            txt += bcolors.OKBLUE + r[1] + bcolors.ENDC + " -->> "
                            txt += bcolors.WARNING + r[2] + bcolors.ENDC + " -->> "

                            txt2 =  r[0] + " -->> "
                            txt2 += r[1] + " -->> "
                            txt2 += r[2] + " -->> "

                            meta = "patent_id="+id_+"--==--"
                            meta += "sentence="+str(count)+"--==--"
                            meta += "organization="+self.data[id_]["organization"]+"--==--"
                            meta += "date="+self.date
                            print txt + meta
                            self.F_is.write("RELATION="+txt2+meta+"\n")
                        print "------------------"
                        self.F_is.write("------------------"+"\n")
        self.F_is = open(self.save_dir+self.name+"/finished.data","w")
        # sys.exit(1)









        ##
