import sys,glob,os
import getpass
import logging
import datetime
import cPickle
import spacy
import numpy as np
import string
import gensim
from gensim.utils import lemmatize

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

class relation_is_a():
    """docstring for spacy_chunks."""
    def __init__(self, logger):
        self.logger = logger
        self.user = getpass.getuser()
        self.project = "all-wiki"
        self.nlp = spacy.load('en_core_web_sm')

        if os.path.isdir("/media/"+self.user+"/Data4"):
            self.data_dir = "/media/"+self.user+"/Data4/ROSA/analysis/"+self.project+"/"
            self.save_dir = "/media/"+self.user+"/Data4/ROSA/db/"
        elif os.path.isdir("/home/"+self.user+"/Data4"):
            self.data_dir = "/home/"+self.user+"/Data4/ROSA/analysis/2018-08-06-11_45_28-(gas turbine)/"
            self.save_dir = "/home/"+self.user+"/Data4/ROSA/db/all-claims/"
        self.counter = 0

        self.logger.info("learning is-a relations ready..")

    def _process_all(self):
        if not os.path.isdir(self.save_dir):
            os.mkdir(self.save_dir)
        self.save_dir += "is_a/"
        if not os.path.isdir(self.save_dir):
            os.mkdir(self.save_dir)

        self.folders = sorted([x[0] for x in os.walk(self.data_dir)])

        for folder in self.folders:
            self.name = folder.split("/")[-1]
            if not os.path.isdir(self.save_dir+self.name):
                os.mkdir(self.save_dir+self.name)

            for file_ in sorted(glob.glob(folder+"/*.p")):
                self.file_name = file_.split("/")[-1].replace(".p",".txt")
                if os.path.isfile(self.save_dir+self.name+"/"+self.file_name):
                    continue
                self.logger.info("processing "+self.name+"-"+self.file_name)
                self.wiki_page = cPickle.load( open(file_, "r") )
                self._process_chunks()

    def _process_chunks(self):
        # self.F_is = open(self.save_dir+self.name+"/"+self.file_name,"w")
        self.unique_chunks = []
        self.unique_codes = {}
        self.bad_starting_words = ["and", "or"]
        for id_ in self.wiki_page:
            self._1_is_a_relations(id_)


    def _clean_cnk(self, txt):
        txt = txt.replace("&lt;sub&gt;","",-1)
        return txt

    def _clean_words(self, words):
        for i in range(len(words)):
            words[i][0] = words[i][0].replace("&lt;sub&gt;"," ",-1)
            words[i][0] = words[i][0].replace("&lt;/sub&gt;"," ",-1)
            words[i][0] = words[i][0].replace("&lt;/sub&gt"," ",-1)
            words[i][0] = words[i][0].replace("  "," ",-1)
            words[i][0] = words[i][0].replace("  "," ",-1)
        return words

    def _clean_new_code(self, code, not_ok, c):
        code+="-"
        counter = 0
        while 1:
            cool = 1
            for i in not_ok:
                if " "+i+" -" in code:
                    code = code.replace(" "+i+" -", " -")
                    new_c = i
                    cool = 0
                    counter += 1
            if cool:
                break
        code = code[:-1]
        if counter:
            code += new_c
        else:
            code += c
        return code

    def _get_code_mask(self, code):
        code_mask = ""
        for ch in code:
            if ch == " ":
                code_mask += ch
            else:
                code_mask += "_"

        c1 = code.split(" ")
        c2 = code_mask.split(" ")
        c2[0] = c1[0]
        c2[-1] = c1[-1]
        code_mask = " ".join(c2)
        return code_mask

    def _extract_uni_concept(self, text, tags):
        ok_ = ["JJ", "NN", "NNS", "NNP"]+["\'\'".decode('utf8'), "``".decode('utf8')]
        ok_middle_ = ok_
        not_ok_end_ = ["RB", "IN", "DT", "JJ"]

        codes = []
        new_code = ""
        pre_code = ""

        for c in tags:
            if new_code != "" and c in ok_middle_:
                new_code += c+" "
            elif c in ok_:
                new_code = pre_code+" "+c+" "
            else:
                if new_code != "":
                    new_code = self._clean_new_code(new_code, not_ok_end_, c)
                    if len(new_code.split(" ")) > 2:
                        if new_code not in codes:
                            codes.append(new_code)
                    new_code = ""
                pre_code = c
        print codes
        print "-----------"

        no_code_deteceted_flag = 1
        c = " ".join(tags)
        s = text
        for c_code,code in enumerate(codes):
            c_code = 0
            code_mask = self._get_code_mask(code)
            while(1):
                if code in c:
                    no_code_deteceted_flag = 0
                    self.sentences[count]["flag"] = 1
                    ind1 = len(c[0:c.index(code)].split(" "))
                    term = s[ind1-1:ind1-1+len(code.split(" "))]
                    phrase = " ".join(term[1:-1])
                    if phrase not in self.concepts:
                        self.concepts[phrase] = {}
                        self.concepts[phrase]["count"] = 0
                        self.concepts[phrase]["sentn"] = []
                        for word in phrase.split(" "):
                            if word not in self.unique_concept_words:
                                self.unique_concept_words.append(word)
                    self.concepts[phrase]["count"] += 1
                    self.concepts[phrase]["sentn"].append(count)
                    index = [ind1,ind1-2+len(code.split(" "))]
                    for i in range(index[0],index[1]):
                        self.sentences[count]["annos"][i] = c_code+1
                    c = c.replace(code, code_mask,1)
                else:
                    break
        # this is to reset the annotations for the sentence
        if no_code_deteceted_flag:
            l = len(c.split(" "))
            self.sentences[count]["annos"] = np.zeros((l),dtype=np.int)
            self.sentences[count]["display"] = " ".join(s)


        sys.exit(1)
        # self._extract_concepts_from_one_sentence(count, codes)

    def _1_is_a_relations(self, id_):
        for p_num in self.wiki_page[id_]["spacy_chunks"]:
            if " is a " in self.wiki_page[id_]["text"][p_num]:
                para = self.wiki_page[id_]["text"][p_num] #.replace(" is a ", bcolors.FAIL + " is a " + bcolors.ENDC,-1)
                doc = self.nlp(para.decode('utf8'))
                text, tags = [],[]
                for token in doc:
                    text.append(token.text)
                    tags.append(token.tag_)
                print tags
                print
                print text
                print
                self._extract_uni_concept(text, tags)


                #
                # # print para
                # for count in range(len(self.wiki_page[id_]["spacy_chunks"][p_num])) :
                #     cnk = self.wiki_page[id_]["spacy_chunks"][p_num][count]
                #     c, id1, id2, _ = cnk
                #     if doc[id2].text == "is" and doc[id2+1].text == "a":
                #         print para.replace(" is a ", bcolors.FAIL + " is a " + bcolors.ENDC,-1)
                #         print
                #         print cnk
                #         print self.wiki_page[id_]["spacy_chunks"][p_num][count+1]
                #         print
                #         print ttt
                #
                # # print bcolors.FAIL + phrase + bcolors.ENDC, "-->", "can be", "-->",
                # print bcolors.OKBLUE + prop + bcolors.ENDC, "<-->",
                # print bcolors.WARNING + meta_data + bcolors.ENDC
        # print self.wiki_page[id_].keys()
        # if " is a " in self.wiki_page[id_]["text"]:
        #     print self.wiki_page[id_]["text"]
        #     print "-----------------------"
        #     print
        # for p_num in self.wiki_page[id_]["spacy_chunks"]:
        #     print id_,p_num
        #     for cnk in self.wiki_page[id_]["spacy_chunks"][p_num]:
        #         if "ic " in cnk[0]:
                    # print cnk[0]
                    # cnk[0] = self._clean_cnk(cnk[0])
                    # if cnk[0] not in self.unique_chunks:
                    #     # if "MIN" not in cnk[0]:
                    #     #     continue
                    #     print cnk[0]
                    #     s,e = cnk[1], cnk[2]
                    #     noun = []
                    #     properties = []
                    #
                    #     words = self.wiki_page[id_]["spacy_data"][p_num][s:e]
                    #     words = self._clean_words(words)
                    #
                    #     for j in range(len(words)):
                    #         ## bleed air
                    #         if "ic" == words[j][0][-2:] and words[j][2] != "NOUN":
                    #             # Ni-based
                    #             if "HYPH" == words[j-1][3]:
                    #                 ww = words[j-2][0] + words[j-1][0] + words[j][0]
                    #                 properties.append(ww)
                    #
                    #             # # nickel based
                    #             # elif words[j][0] == "based":
                    #             #     ww = words[j-1][0] + " " + words[j][0]
                    #             #     properties.append(ww)
                    #
                    #             #circumferentially spaced
                    #             elif "ly" == words[j-1][0][-2:]:
                    #                 ww = words[j-1][0] + " " + words[j][0]
                    #                 properties.append(ww)
                    #
                    #             else:
                    #                 properties.append(words[j][0])
                    #             # a strip-shaped or band-shaped material
                    #             noun = []
                    #
                    #         elif properties != []: # and words[j][0] not in string.punctuation:# and j[2] in ["NOUN", "ADJ"]:
                    #             if noun == [] and words[j][0] in self.bad_starting_words:
                    #                 continue
                    #             noun.append(words[j][0])
                    #
                    #
                    #     if noun != []:
                    #         phrase = " ".join(noun)
                    #         ## cleaning the phrase
                    #         for i in "/!$%^&*()+_-":
                    #             phrase = phrase.replace(" "+i+" ",i,-1)
                    #
                    #         if " and " in phrase:
                    #             ph = phrase.split(" ")
                    #             ind = ph.index("and")
                    #             # limited relative radial and circumferential motion
                    #             if ph[ind-1][-3:] == "ial" and ph[ind+1][-3:]=="ial":
                    #                 phrase1 = " ".join(ph[0:ind]+ph[ind+2:])
                    #                 phrase2 = " ".join(ph[0:ind-1]+ph[ind+1:])
                    #                 phrases = [phrase1, phrase2]
                    #             # the desired state and decreasing flow area
                    #             else:
                    #                 phrases = phrase.split(" and ")
                    #         else:
                    #             phrases = [phrase]
                    #
                    #         for phrase in phrases:
                    #             try:
                    #                 while phrase[0] in ",.-_ ":
                    #                     phrase = phrase[1:]
                    #
                    #                 ## print the learned graph
                    #                 meta_data = self.wiki_page[id_]["id"] + "," + self.wiki_page[id_]["title"]
                    #                 # meta_data += "," + self.wiki_page[id_]["url"]
                    #
                    #                 for prop in properties:
                    #                     ## filter out some of the weird properties
                    #                     if ")" in prop or "(" in prop or "=" in prop:
                    #                         continue
                    #                     ## make sure that the sentence is ascii
                    #                     if not self._is_ascii(prop) or not self._is_ascii(phrase):
                    #                         continue
                    #
                    #                     print bcolors.FAIL + phrase + bcolors.ENDC, "-->", "can be", "-->",
                    #                     print bcolors.OKBLUE + prop + bcolors.ENDC, "<-->",
                    #                     print bcolors.WARNING + meta_data + bcolors.ENDC
                            #
                            #             self.F_is.write(phrase + " --> is a --> " + prop + " <--> " + meta_data + "\n")
                            #     except:
                            #         print ">>>>>>>>>>>>>>>>>>>>>>>>>> bad phrase"+phrase
                            #                 # print lemmatize(phrase+' can be '+prop)
                            # print "----------------"


    def _is_ascii(self, s):
        return all(ord(c) < 128 for c in s)
