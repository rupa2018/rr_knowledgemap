from gensim.test.utils import common_texts
from gensim.corpora.dictionary import Dictionary
from gensim.test.utils import datapath
from gensim import corpora
import gensim
from nltk.corpus import stopwords
from gensim import utils
# import cPickle
import logging
import glob
import os
from multiprocessing import Pool
import sys
import json
import gc
import numpy as np

from nltk.stem import PorterStemmer
import time
import re

################################################################################
STOP_WORDS = stopwords.words('english')
def _tokenize_data(data, token_max_len=40, lower=True):
    ps = PorterStemmer()
    Factor = 1
    """
    Tokenize a piece of text from wikipedia. The input string `content` is assumed
    to be mark-up free (see `filter_wiki()`).
    Set `token_min_len`, `token_max_len` as character length (not bytes!) thresholds for individual tokens.
    Return list of tokens as utf8 bytestrings.
    """
    # TODO maybe ignore tokens with non-latin characters? (no chinese, arabic, russian etc.)
    data_stem = " ".join( [ ps.stem(i) for i in data.split(" ") ] )
    content = data
    content = content.lower()
    content = ''.join([i for i in content if i.isalnum() or i==" "])
    # content = ''.join([i for i in content if not i.isdigit()])
    while "  " in content:  content = content.replace("  "," ",-1)
    content = content.split(" ")

    ## add unigrams
    content = [gensim.utils.to_unicode(ps.stem(i)) for i in content if len(i)<= token_max_len and len(i)>1 and i not in STOP_WORDS]
    tokens = content

    ## add bigrams
    for i in range(len(content)-1):
        phrase = " ".join(content[i:i+2])
        if phrase in data_stem:
            phrase = gensim.utils.to_unicode(phrase)
            tokens += [phrase]*Factor
    return tokens

class Index_data():
    """docstring for Index_data."""
    def __init__(self):
        self.partition = 220          # divide the data into N partitions
        self.save_dir_ = "/home/mo/GENSIM/wiki/"
        if not os.path.isdir(self.save_dir_):
            os.mkdir(self.save_dir_)

        for i in range(self.partition):
            if not os.path.isdir(self.save_dir_+str(i)+"/"):
                os.mkdir(self.save_dir_+str(i)+"/")

        # logging data
        self.logger = logging.getLogger("Index data")
        logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s')
        logging.root.setLevel(level=logging.INFO)
        self.logger.info("running data indexing..")

        # finding csv files of claims
        if os.path.isdir("/media/mo/Data4/wiki/extracted_files/"):
            self.data_dir_ = "/media/mo/Data4/wiki/extracted_files/"
        else:
            self.logger.info("check your directories..")
            sys.exit(1)

        self.p = Pool(15)

    def _process_files(self):
        self.data = []
        for folder in sorted([x[0] for x in os.walk(self.data_dir_)]):
            self.data += glob.glob(folder+"/wiki_*")
        self.data = sorted(self.data)
        data_chukns = self._chunkify(self.data, self.partition)
        for count, data in enumerate(data_chukns):
            # print(count)
            # if count<36:
            #     continue
            self.name_ = str(count)
            self._read_files(data)
            self._tokenize()
            self._tfidf()
            self._index()
            self._test()
            # release memory
            gc.collect()

    def _chunkify(self, lst, n):
        return [lst[i::n] for i in range(n)]

    def _utf(self, text):
        re.sub(r'\W+', '', text)
        return text

    def _read_files(self, data):
        self.documents = []
        self.documents_to_save = []
        for counter, file_ in enumerate(data):
            f = open(file_, "r")
            for counter2,line in enumerate(f):
                line = line.split("\n")[0]
                if line != "":
                    meta = file_.split("/")[-2:] + [str(counter2)]
                    ind = "/".join(meta)
                    doc = line
                    if len(line)>100 and "<doc id=" not in line:
                        self.documents.append([self._utf(doc), ind])
                        self.documents_to_save.append(ind)
            f.close()
        json.dump(self.documents_to_save, open(self.save_dir_+self.name_+"/data.json", "w"))

    def _tokenize(self):
        ################################################################################
        self.logger.info("  -tokenize data")
        ## multiprocessing
        data = [i[0] for i in self.documents]
        common_texts = self.p.map(_tokenize_data, data)

        self.logger.info("  -creating dictionary")
        self.dictionary = Dictionary(common_texts, prune_at=250000)
        self.dictionary.filter_extremes(no_below=1, no_above=0.5, keep_n=250000)
        self.features = len(self.dictionary.items())
        self.dictionary.save(self.save_dir_+self.name_+'/dictionary.dict')

        self.logger.info("  -creating corpus")
        self.corpus = [self.dictionary.doc2bow(text) for text in common_texts]
        # corpora.MmCorpus.serialize(save_dir_+'test.mm', corpus)

    def _tfidf(self):
        ################################################################################
        ## tfidfi
        self.logger.info("  -creating tfidf")
        self.tfidf = gensim.models.TfidfModel(self.corpus) # step 1 -- initialize a model
        self.tfidf.save(self.save_dir_+self.name_+'/tfidf.tfidf')

        self.corpus_tfidf = self.tfidf[self.corpus]

        # if LDA:
        #     # Train the model on the corpus.
        #     lda = gensim.models.LdaModel(corpus_tfidf, id2word=dictionary, num_topics=4)

    def _index(self):
        ################################################################################
        ####### indexing
        self.logger.info("  -creating index")

        # if LDA:
        #     index = gensim.similarities.Similarity(save_dir_, lda[corpus_tfidf], num_features=features)
        # else:
        self.index = gensim.similarities.Similarity(self.save_dir_+self.name_+'/', self.corpus_tfidf, num_features=self.features)
        self.index.save(self.save_dir_+self.name_+'/index.index')

    ################################################################################
    def _test(self):
        # question = "how many blades in Trent 900?"
        # question = "which aircraft uses Trent xwb?"
        # question = "which document specifies all the required attributes of the Trent XWB fan system?"
        question = "New York"
        paragraphs = self._query(question)
        # self._cape_interface(question, paragraphs)

    def _query(self, q_doc):
        ####### Similarity test
        return_num = 3          # the number of paragraphs returned
        search_num = 25         # apply the Tversky distance on these and return only the return_num

        start = time.time()
        dictionary = Dictionary.load(self.save_dir_+self.name_+'/dictionary.dict')
        index = gensim.similarities.MatrixSimilarity.load(self.save_dir_+self.name_+'/index.index')
        tfidf = gensim.models.TfidfModel.load(self.save_dir_+self.name_+'/tfidf.tfidf') # step 1 -- initialize a model
        documents = json.load(open(self.save_dir_+self.name_+"/data.json", "r"))
        end = time.time()
        print("TIME IS: >>>>>>>>>>>> ",end - start)

        tokens = _tokenize_data(q_doc)
        vec_bow = dictionary.doc2bow(tokens)
        vec_test = tfidf[vec_bow]

        sims = index[vec_test]
        sims = list(enumerate(sims))
        sims = list(reversed(sorted(sims, key=lambda tup: tup[1])))[:search_num]
        ps = PorterStemmer()

        results = [None] * search_num
        for count_, doc in enumerate(sims):
            score = doc[1]
            category, doc, line = documents[doc[0]].split("/")
            file_ = "/media/mo/Data4/wiki/extracted_files/"+category+"/"+doc
            f = open(file_, "r")
            lines = f.readlines()
            claim_text = lines[int(line)]
            claim_text = claim_text.split("\n")[0]
            claim_text_stemmed = " "+" ".join( [ ps.stem(i) for i in claim_text.split(" ") ] )+" "
            f.close()
            #NOTE include this in your search test somehow
            ## this is called Tversky distance
            counter = 0
            for tt in tokens:
                if " "+tt+" " in claim_text_stemmed:
                    counter += 1
            score = 0.3*score + 0.7*counter/float(len(tokens))
            results[count_] = [score, claim_text, doc, line]

        results.sort(key=lambda x: x[0])
        results = list(reversed(results))

        for R in results[:return_num]:
            print(R[0])
            print(R[1])
            print()

        print("_---------------------------------_")
        return results[:return_num]



    ################################################################################
    def _cape_interface(self, question, paragraphs):
        print("asking cape for an answer")
        results = []
        for counter, para in enumerate(paragraphs):
            print("  - asking about paragprah number:"+str(counter))
            # print(para)
            data = para[1]
            score = para[0]
            d_query = data.replace(" ","+",-1)
            q_query = question.replace(" ","+",-1)

            r = requests.get('http://localhost:5050/api/0.1/answer?token=demo&question='+q_query+'&text='+d_query)
            if r.json()['success']:
                try:
                    print("  - found an answer..")
                    # print(r.json()['result']['items'])
                    answer = r.json()['result']['items'][0]['answerText']
                    confidence = r.json()['result']['items'][0]['confidence']
                    ans_indx_1 = r.json()['result']['items'][0]['answerTextStartOffset']
                    ans_indx_2 = r.json()['result']['items'][0]['answerTextEndOffset']
                    confidence *= score
                    data = data[:ans_indx_1] + bcolors.FAIL + answer + bcolors.ENDC + data[ans_indx_2:]
                    results.append([confidence, answer, data])
                except:
                    print("  - didn't find an answer..")

        results.sort(key=operator.itemgetter(0))
        max_conf = results[-1][0]
        for i in results:
            confidence, answer, data = i
            if confidence > 0.7*max_conf:
                print()
                print("paragraph: "+data)
                print()
                print("The answer is:   (", answer,") with score:", confidence)
                print("------------")



def main():
    I = Index_data()
    I._process_files()

if __name__=="__main__":
    main()
