import requests

## run this first
## docker run -ti -p 5050:5050 -p 5051:5051 bloomsburyai/cape

# data = "Trent 1000 is an airplane engine."
# data = "An axial flow annular discharge duct and flange mounted in said casing connected directly to said low pressure turbine to receive the exhaust therefrom and to provide means for connecting thereto the hot gas generated: in which said low pressure compressor of said hot gas generator contains multiple stages to accomplish a compression ratio no greater than about 3.2 with said single bearing overhung design and in which said high pressure compressor of said hot gas generator contains multiple stages to accomplish an overall compression ratio for both low and high pressure compressors of at least about 35 and wherein said low pressure discharge housing contains a low pressure compressor discharge axial duct and radial diffuser flaring outwardly to said radial discharge flange and wherein said low pressure return housing contains an \"S\" shaped low pressure reverse flow return duct tucked inside and under said low pressure discharge duct wherein adequate volumetric dimensions are provided with the radii of said r2 and r3 and compressor stub shaft to yield a return velocity inside and return duct of about 75 feet per second therein resulting typically in a 0.25 percent pressure loss"

data = "The Rolls-Royce Trent 1000 is a British turbofan engine, developed from earlier Trent series engines. The Trent 1000 powered the Boeing 787 Dreamliner on its maiden flight, and on its first commercial flight."
d_query = data.replace(" ","+",-1)

# question = "what is trent 1000?"
question = "which engine does the Boeing 787 uses?"
q_query = question.replace(" ","+",-1)

r = requests.get('http://localhost:5050/api/0.1/answer?token=demo&question='+q_query+'&text='+d_query)
print(r.json()['success'])
# print(r.json()['result']['items'][0].keys())
# print("The questios is: ",question)
answer = r.json()['result']['items'][0]['answerText']
confidence = r.json()['result']['items'][0]['confidence']
print("The answer is:   ", answer, confidence)
