import requests
import logging
from gensim.test.utils import common_texts
from gensim.corpora.dictionary import Dictionary
from gensim.test.utils import datapath
from gensim import corpora
import gensim
from nltk.corpus import stopwords
from gensim import utils
import json
import operator

################################################################################
STOP_WORDS = stopwords.words('english')
def _tokenize_data(data, token_max_len=40, lower=True):
    ps = PorterStemmer()
    Factor = 1
    """
    Tokenize a piece of text from wikipedia. The input string `content` is assumed
    to be mark-up free (see `filter_wiki()`).
    Set `token_min_len`, `token_max_len` as character length (not bytes!) thresholds for individual tokens.
    Return list of tokens as utf8 bytestrings.
    """
    # TODO maybe ignore tokens with non-latin characters? (no chinese, arabic, russian etc.)
    data_stem = " ".join( [ ps.stem(i) for i in data.split(" ") ] )
    content = data
    content = content.lower()
    content = ''.join([i for i in content if i.isalnum() or i==" "])
    # content = ''.join([i for i in content if not i.isdigit()])
    while "  " in content:  content = content.replace("  "," ",-1)
    content = content.split(" ")

    ## add unigrams
    content = [gensim.utils.to_unicode(ps.stem(i)) for i in content if len(i)<= token_max_len and len(i)>1 and i not in STOP_WORDS]
    tokens = content

    ## add bigrams
    for i in range(len(content)-1):
        phrase = " ".join(content[i:i+2])
        if phrase in data_stem:
            phrase = gensim.utils.to_unicode(phrase)
            tokens += [phrase]*Factor
    return tokens


# def searchClaimParallel(doc, model_name):
#     try:
#         returned = {}
#         # for model_name in model_names:
#         num = 5       # the maximum number of results returned
#         print("searching mode"+model_name)
#
#         # date = model_name.split("/")[-1].split(".")[0]
#         dictionary = gensim.corpora.Dictionary.load(model_name+'/dictionary.dict')
#         index = gensim.similarities.MatrixSimilarity.load(model_name+'/index.index')
#         tfidf = gensim.models.TfidfModel.load(model_name+'/tfidf.tfidf') # step 1 -- initialize a model
#         documents = json.load(open(model_name+"/data.json", "r"))
#         tokens = _tokenize_claims([doc])
#         vec_bow = dictionary.doc2bow(tokens)
#         vec_test = tfidf[vec_bow]
#
#         sims = index[vec_test]
#         sims = list(enumerate(sims))
#         sims = list(reversed(sorted(sims, key=lambda tup: tup[1])))[:num]
#
#         title = "---"
#         firm = "000"
#         inventor = "111"
#         for count, rr in enumerate(sims):
#             score = rr[1]
#             claim_text, meta = documents[rr[0]]
#
#             counter = 0
#             ## this is called Tversky distance
#             for tt in tokens:
#                 if tt in claim_text:
#                     counter += 1
#             score = 0.3*score + 0.7*counter/float(len(tokens))
#
#             _, patent_id, _, claim_num = meta
#             returned[model_name.split("/")[-1]+"-"+str(count)] = {"score":int(score*100000), "patent_id":patent_id, "title":title, "firm":firm,
#                                                                   "inventor":inventor, "claim_num":claim_num, "claim_text":claim_text, "date":date}
#
#         del dictionary
#         del index
#         del tfidf
#         del documents
#         del sims
#         del score
#         gc.collect()
#
#         return returned
#     except:
#         pass

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

class ask_cape():
    """docstring for ask_cape."""
    def __init__(self):
        self.logger = logging.getLogger("search with cape")
        logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s')
        logging.root.setLevel(level=logging.INFO)
        self.logger.info("running search engine trying to answer:")

        self.dir_ = "/media/mo/Data4/wiki/GENSIM_partition/0/"

        self._load_data()

    def _load_data(self):
        self.dictionary = Dictionary.load(self.dir_+'dictionary.dict')
        self.index = gensim.similarities.MatrixSimilarity.load(self.dir_+'index.index')
        self.tfidf = gensim.models.TfidfModel.load(self.dir_+'tfidf.tfidf') # step 1 -- initialize a model
        self.documents = json.load(open(self.dir_+"data.json", "r"))

    def _query(self, doc):
        ####### Similarity test
        return_num = 3
        print("performing similarity test")
        dictionary = self.dictionary
        index = self.index
        tfidf = self.tfidf
        documents = self.documents

        tokens = self._tokenize([doc])
        print(">>>>>>>>>>>>>>>",tokens)
        vec_bow = dictionary.doc2bow(tokens)
        vec_test = tfidf[vec_bow]

        print("---------------")

        sims = index[vec_test]
        sims = list(enumerate(sims))
        sims = list(reversed(sorted(sims, key=lambda tup: tup[1])))[:15]

        results = []
        for doc in sims:
            score = doc[1]
            print("FIX MEEEE HERE")
            print("change the tokenize function in here and in index wiki to the same one in km_test")
            print(documents[doc[0]])
            name, pdf, doc, line = documents[doc[0]]
            f = open(doc, "r")
            lines = f.readlines()
            claim_text = lines[line]

            #NOTE include this in your search test somehow
            ## this is called Tversky distance
            counter = 0
            for tt in tokens:
                if tt in claim_text:
                    counter += 1
            score = 0.3*score + 0.7*counter/float(len(tokens))
            results.append([score, claim_text, doc, line])

        results.sort(key=lambda x: x[0])
        results = list(reversed(results))

        for R in results[:return_num]:
            print(R[0])
            print(R[1])
            print()

        print("_---------------------------------_")
        return results[:return_num]

    ################################################################################
    def _cape_interface(self, question, paragraphs):
        print("asking cape for an answer")
        results = []
        for counter, para in enumerate(paragraphs):
            print("  - asking about paragprah number:"+str(counter))
            # print(para)
            data = para[1]
            score = para[0]
            d_query = data.replace(" ","+",-1)
            q_query = question.replace(" ","+",-1)

            r = requests.get('http://localhost:5050/api/0.1/answer?token=demo&question='+q_query+'&text='+d_query)
            if r.json()['success']:
                try:
                    print("  - found an answer..")
                    # print(r.json()['result']['items'])
                    answer = r.json()['result']['items'][0]['answerText']
                    confidence = r.json()['result']['items'][0]['confidence']
                    ans_indx_1 = r.json()['result']['items'][0]['answerTextStartOffset']
                    ans_indx_2 = r.json()['result']['items'][0]['answerTextEndOffset']
                    confidence *= score
                    data = data[:ans_indx_1] + bcolors.FAIL + answer + bcolors.ENDC + data[ans_indx_2:]
                    results.append([confidence, answer, data])
                except:
                    print("  - didn't find an answer..")

        results.sort(key=operator.itemgetter(0))
        max_conf = results[-1][0]
        for i in results:
            confidence, answer, data = i
            if confidence > 0.7*max_conf:
                print()
                print("paragraph: "+data)
                print()
                print("The answer is:   (", answer,") with score:", confidence)
                print("------------")


    # def _query_wiki(self):
    #     print("look at the code and change this function with the other query function....")
    #     sys.exit(1)
    #     ################################################################################
    #     ####### Similarity test
    #     self.logger.info("performing similarity test")
    #     doc = self.question
    #     vec_bow = self.dictionary.doc2bow(_tokenize([doc]))
    #     vec_test = self.tfidf[vec_bow]
    #
    #     sims = self.index[vec_test]
    #     sims = list(enumerate(sims))
    #     sims = list(reversed(sorted(sims, key=lambda tup: tup[1])))[:3]
    #
    #     self.paragraphs = []
    #     for result in sims:
    #         score = result[1]
    #         if score > 0 and result[0]<len(self.documents):
    #             try:
    #                 print(result)
    #                 print(self.documents[result[0]][0])
    #                 file_, sent = self.documents[result[0]][0].split("--==--")
    #                 f = open(file_,"r")
    #                 lines = f.readlines()
    #                 print(lines[int(sent)])
    #                 if "<doc id=" in lines[int(sent)]:
    #                     continue
    #                 data = lines[int(sent)-2]+" "+lines[int(sent)]+" "+lines[int(sent)+2]+" "+lines[int(sent)+4]
    #                 data = data.replace("\n","",-1)
    #                 data_print = data
    #                 for w in self.question.split(" "):
    #                     data_print = data_print.replace(w, bcolors.FAIL + w + bcolors.ENDC, -1)
    #                 print(data_print)
    #                 print("------")
    #                 self.paragraphs.append( [data, self.documents[result[0]][0], score] )
    #             except:
    #                 print("something went wrong ->",self.documents[result[0]])

    # def _cape_interface(self):
    #     print("look at the code and change this function with the other query function....")
    #     sys.exit(1)
    #     self.logger.info("asking cape for an answer")
    #     question = self.question
    #     results = []
    #     for counter, para in enumerate(self.paragraphs):
    #         self.logger.info("  - asking about paragprah number:"+str(counter))
    #         # print(para)
    #         data = para[0]
    #         score = para[2]
    #         d_query = data.replace(" ","+",-1)
    #         q_query = question.replace(" ","+",-1)
    #
    #         r = requests.get('http://localhost:5050/api/0.1/answer?token=demo&question='+q_query+'&text='+d_query)
    #         if r.json()['success']:
    #             try:
    #                 answer = r.json()['result']['items'][0]['answerText']
    #                 confidence = r.json()['result']['items'][0]['confidence']
    #                 confidence *= score
    #                 data = data.replace(answer, bcolors.FAIL + answer + bcolors.ENDC, -1)
    #                 results.append([confidence, answer, data])
    #             except:
    #                 pass
    #
    #     results.sort(key=operator.itemgetter(0))
    #     for i in results:
    #         confidence, answer, data = i
    #         print()
    #         print("paragraph: "+data)
    #         print()
    #         print("The answer is:   ", answer, confidence)
    #         print("------------")

def main():
    A = ask_cape()
    while(1):
        question = input("Enter a question: ")
        A.question = question
        # print(question)
        # print("--------------")
        A._query(question)
        # A._cape_interface()

if __name__=="__main__":
    main()
