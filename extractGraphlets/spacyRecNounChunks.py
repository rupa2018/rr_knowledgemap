import spacy

nlp = spacy.load("en_core_web_sm")

doc = nlp("The blades are usually made of Nickel-based super alloys for HP compressors with operating temperature exceeding 5000C value.")

for np in doc.noun_chunks: # use np instead of np.text
    print(np)

print()

# code to recursively combine nouns
# 'We' is actually a pronoun but included in your question
# hence the token.pos_ == "PRON" part in the last if statement
# suggest you extract PRON separately like the noun-chunks above

index = 0
nounIndices = []
for token in doc:
    # print(token.text, token.pos_, token.dep_, token.head.text)
    if token.pos_ == 'NOUN':
        nounIndices.append(index)
    index = index + 1


print(nounIndices)
for idxValue in nounIndices:
    doc = nlp("The blades are usually made of Nickel-based super alloys for HP compressors with operating temperature exceeding 5000C.")
    span = doc[doc[idxValue].left_edge.i : doc[idxValue].right_edge.i+1]
    span.merge()

    print("\r\n")
    for token in doc:
        if token.dep_ == 'dobj' or token.dep_ == 'pobj' or token.pos_ == "PRON" or token.dep_ =='nsubj' or token.dep_== 'nsubjpass':
            print(token.text, token.dep_, token.head.text,
                [left for left in token.lefts], [right for right in token.rights], [child for child in token.children], [ancestor for ancestor in token.ancestors])
for root, dirs, files in os.walk("/home/rupa/ROSA/graphlets", topdown=False):
    for name in files:
        fileName = os.path.join(root, name)