#!/usr/bin/env python3.5
# coding: utf8
'''
110908: calls extractGraphlets_v5 to get graphlets from all sentences in a textfile.

13/09/18: v2: includes GUI

'''

import sys
import spacy
from spacy.tokens import Token
from spacy import displacy
from extractGraphlets_v7 import extractGraphlets
import networkx as nx
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import interactive
import tkinter as tk
from tkinter import *
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure
from tkinterFns import drawGUI
import numpy as np
try:
    import pygraphviz
    from networkx.drawing.nx_agraph import graphviz_layout
except ImportError:
    try:
        import pydotplus
        from networkx.drawing.nx_pydot import graphviz_layout
    except ImportError:
        raise ImportError("This example needs Graphviz and either "
                              "PyGraphviz or PyDotPlus")
#from networkx.drawing.nx_agraph import graphviz_layout



class graphletsFromText():
    def __init__(self, textFile, query=''):
        nlp = spacy.load('en_core_web_sm')
        self.fileName = textFile
        self.text = self.readFile()
        self.spacyText = nlp(self.text)
        self.graphlets = self.sendSents()
        self.graphlets1 = self.removeDet()
        self.G, self.edge_labels = self.drawGraph()
        self.genGUI()

    def genGUI1(self):
        master=Tk()
        guiObj = drawGUI(master, self.graphlets1)
        # draw networkx plot
        fig = plt.figure(figsize=(5,4))  
        pos = nx.string_layout(self.G)
        nx.draw(self.G, pos, node_size = 100)
        nx.draw_networkx_labels(self.G, pos, font_size = 10)
        nx.draw_networkx_edge_labels(self.G, pos, self.edge_labels, font_size = 10)
        guiObj.displayFig(fig)
        #get querybrew
        #print(guiObj.text)
        #queryTerm = guiObj.text
        #print(queryTerm)
        master.mainloop()

    def genGUI(self):
        master=Tk()
        guiObj = drawGUI(master, self.graphlets1)
        # draw networkx plot
        fig = plt.figure(figsize=(20,15))  
        pos = graphviz_layout(self.G, prog = 'neato')
        nx.draw(self.G, pos, node_size = 100)
        nx.draw_networkx_labels(self.G, pos, font_size = 10)
        nx.draw_networkx_edge_labels(self.G, pos, self.edge_labels, font_size = 10)
        guiObj.displayFig(fig)
        #get query

        master.mainloop() 


    def readFile(self):
        with open(self.fileName, 'r') as in_file:
                text = in_file.read()
                return text

    def sendSents(self):
        graphlets = []
        for count, sent in enumerate(self.spacyText.sents):
            #print("Sentence {}".format(count))
            #print(sent.string.strip())
            exGrObj = extractGraphlets(sent.string.strip(),drawGraphFlag=False)
            graphlets.extend(exGrObj.outputGraphlets)
        #print("\r\n")
        graphletsT=[]
        for gr in graphlets: # get unique graphlets
            graphletsT.append(tuple(gr))
        return list(set(graphletsT))
       
    def removeDet(self):
        removal_list = ["the", "a", "an", "The", "A", "An"]
        graphlets1 = []
        for gr in self.graphlets:  
            gr1=[]
            for entity in gr: 
                edit_string_as_list = entity.split()
                final_list = [word for word in edit_string_as_list if word not in removal_list]
                final_string = ' '.join(final_list)
                gr1.append(final_string)
            graphlets1.append(gr1)
        return graphlets1

    def drawGraph(self):
        G = nx.DiGraph()
        edge_labels={}
        for gr in self.graphlets1:
            #print(gr[0])
            #print(gr[1])
            #print(gr[2])
            #print("\r\n")
            G.add_edge(gr[0], gr[2])
            edge_labels[(gr[0], gr[2])] = gr[1]
        return G, edge_labels

    def _getSubNodes(self, query):
        subNodes = [gr for gr in self.graphlets1 if any(x in query for x in gr)]
        #print("\r\n")
        #print("the query graphlets are:")
        #print("\r\n")
        #print(subNodes)
        if subNodes:
            SG = nx.DiGraph()
            edge_labels={}
            for sn in subNodes:
                SG.add_edge(sn[0], sn[2])
                edge_labels[(sn[0], sn[2])] = sn[1]
            pos = nx.circular_layout(SG)
            nx.draw(SG, pos)
            nx.draw_networkx_labels(SG, pos)
            nx.draw_networkx_edge_labels(SG, pos, edge_labels)
            plt.figure()
            plt.show(block=False) 
            
def main():
        textFile = '/home/rupa/patentInvalidation/development/DataKM/drawingDescription/example.txt'
        graphletsFromText(textFile, query = 'shaft')
        

if __name__ == "__main__":
    main()
