#!/usr/bin/env python3.5
# coding: utf8
 

import networkx as nx 
import pylab as py

sentence = [['basic operation', 'of', 'gas turbine engine'], 'is', ['Brayton cycle', 'with', ['air', 'as', 'working fluid']]]
a = 'The basic operation'
b = 'of'
c = 'the gas turbine engine'
d = 'is'
e = 'a Brayton cycle'
f = 'with'
g = 'air'
h = 'as'
i = 'the working fluid'


G = nx.Graph()

H = nx.Graph()
H.add_edges_from([(g,i)])

I = nx.Graph()
I.add_edges_from([(a,c)])


G.add_edge(H,I)

Gpos = nx.spring_layout(G)
Hpos = nx.spring_layout(H)
Ipos = nx.spring_layout(I)

scalefactor = 0.1
for node in H.nodes():
    Hpos[node] = Hpos[node]*scalefactor + Gpos[H]

for node in I.nodes():
    Ipos[node] = Ipos[node]*scalefactor + Gpos[I]


nx.draw_networkx_edges(G, pos = Gpos)
nx.draw_networkx_nodes(G, pos = Gpos, node_color = 'b', node_size = 15000, alpha = 0.5)
nx.draw(H, pos = Hpos, with_labels = True)
nx.draw(I, pos = Ipos, with_labels = True)
py.savefig('tmp.png')
