#!/usr/bin/env python3.5
# coding: utf8

''' extracts graphlets from sentence using spacy's noun_chunks and dependency parsing


    v1: 08/08/18: uses networkx to find shortest path between two terms
    v5: working version. Always make sure that this version works
    11/09/18: Added reparseNounChunks(), which changes the parsed doc so that all words in a noun chunk are contained in the same token
    19/09/18: only use noun_chunks from image description if they are followed by a number (indicating that this is an entity of interest)

''' 
import spacy
import networkx as nx
from spacy.tokens import Token
from spacy import displacy
import numpy as np
import pandas as pd
import itertools as it
import matplotlib.pyplot as plt
import sys
import os

print(sys.version_info)

class extractGraphlets():

    def __init__(self, inputSentence, drawGraphFlag=False):
        nlp = spacy.load('en_core_web_sm')
        self.drawGraphFlag = drawGraphFlag
        self.doc = nlp(inputSentence) #input sentence
        self.reparseNounChunks()
        Token.set_extension('said', default = None, force=True)
        self.setTokenExtensions()
        self.graph = self.getNetworkxGraph()
        self.chunkCombs = self.getChunkCombs()
        self.df = self.getGraphlets()
        #print(self.df.to_string())
        self.outputGraphlets = self.getOutputGraphlets()
        #print(self.df.to_string())
        if self.outputGraphlets:
            self.drawGraph()

    def reparseNounChunks(self):
        for noun_phrase in list(self.doc.noun_chunks):
            noun_phrase.merge(noun_phrase.root.tag_, noun_phrase.root.lemma_, noun_phrase.root.ent_type_)
        
    def setTokenExtensions(self):
        for token in self.doc: 
            if token.text == "said":
                for chunk in self.doc.noun_chunks:
                    if token.nbor() in chunk:
                        saidRoot = chunk.root 
                        reqWords = chunk.text.split()
                        try:
                            reqWords.remove("said") # chunk with.split()d" e.g. "gas generator" instead of "said gas generator"
                        except ValueError:
                            pass
                    #find most similar 
                        relevantChunks = []
                        for numWords in range(len(reqWords),0,-1):
                            partReqWords = reqWords[-numWords:] 
                            for chunk in self.doc.noun_chunks:
                                if token.i > chunk.root.i:
                                    if ' '.join(partReqWords) in chunk.text:
                                        relevantChunks.append(chunk)
                                else:
                                    break
                            if relevantChunks:
                                break
                        if relevantChunks:    
                            saidChunk = relevantChunks[-1].root
                            saidRoot._.set('said', saidChunk) 

    def getNetworkxGraph(self):
        edges = []        
        for token in self.doc:         
            if token.dep_ == 'nsubj':
                edges.append((token, token.head))
            if token.rights:
                for right in token.rights:
                    edges.append((token, right))
            if 'nsubj' or 'nsubjpass' in token.head.dep_:
                for ancestor in token.ancestors:
                    edges.append((token, ancestor))
        graph = nx.DiGraph(edges)
        return graph

    def getNetworkxGraph1(self):
        edges = []        
        for token in self.doc:         
            if token.head:
                edges.append((token, token.head))
        graph = nx.Graph(edges)
        return graph

    def getChunkCombs(self):
        #first get noun_chunks, then extract root and root position of each chunk
        chunkRootIndexList=[]
        for chunk in self.doc.noun_chunks:
            try:
                if chunk.root.dep_ == 'nsubj' or chunk.root.nbor().like_num:
                    chunkRootIndexList.append(chunk.root)
            except:
                print("error")
                print(chunk.root.text)
                
        # get all combinations of chunkRootIndexList
        chunkCombs = list(it.combinations(chunkRootIndexList, 2))    
        return chunkCombs

    def getShortestPath(self, chunkCombList):
        try:
            shPath=nx.shortest_path(self.graph, source=chunkCombList[0], target=chunkCombList[1])
            return shPath
        except Exception:
            pass

    def getGraphlets(self):
        #get shortest dependecy path
        indexPD = [chunk.root for chunk in self.doc.noun_chunks]
        noneArray = np.full((len(indexPD), len(indexPD)), None)
        df =pd.DataFrame(data = noneArray, index=indexPD, columns = indexPD)
        for chunkComb in self.chunkCombs:   
            shPath = self.getShortestPath(chunkComb)
            if shPath and len(shPath) > 2:
                shPathEv = self.evaluateShPath(shPath[1:-1], chunkComb)
            else:
                shPathEv = []
            df.loc[chunkComb[0], chunkComb[1]]=shPathEv                     
        return df

    def getOutputGraphlets(self):
        finalGraphlets = []
        for chunkPair in self.chunkCombs:
            if chunkPair[1].dep_ == 'conj':
                concept1T = self.checkConj(chunkPair[1])
                if not self.df.at[concept1T, chunkPair[1]]:
                    relation = []
                else:
                    relation = self.df.loc[concept1T, chunkPair[1]]  
                concept1 = self.getConceptText(concept1T)
                concept2 = self.getConceptText(chunkPair[1])
            else:
                concept1 = self.getConceptText(chunkPair[0])
                concept2 = self.getConceptText(chunkPair[1])
                if not self.df.at[chunkPair[0], chunkPair[1]]:
                    relation = []
                else:
                    relation = self.df.loc[chunkPair[0], chunkPair[1]]
            if concept1 and concept2 and relation:
                relationStr = ' '.join([x.text for x in relation])
                finalGraphlets.append([concept1, relationStr, concept2])
        return finalGraphlets

    def evaluateShPath(self, shPath, chunkComb):      
        #contains rules to extract concepts and relations
        #check if there's another noun chunk
        shPath1=[]
        #print(chunkComb)
        #print(shPath)
        if any(x in shPath for x in [chunk.root for chunk in self.doc.noun_chunks]):
            #find lists          
            if shPath[-1] in [chunk.root for chunk in self.doc.noun_chunks]:
                for item in shPath:
                    if item not in [chunk.root for chunk in self.doc.noun_chunks]:
                        shPath1.append(item)
                    else:
                        break
        else:
            shPath1 = shPath  
        if len(shPath1) > 1 and 'said' not in [word.text for word in shPath1]:
            shPath2 = [shPath1[-1]]
            for count, token in reversed(list(enumerate(shPath1))):
                if token !=shPath1[0]:
                    leftToken = shPath1[count-1]
                if leftToken == token.nbor(-1):
                    shPath2.insert(0,leftToken)
                else:
                    break
        else:
           shPath2 = shPath1
        #print(shPath2)
        return shPath2  

    def drawGraph(self):
        G = nx.DiGraph()
        cCounter=0
        edge_labels={}
        for gr in self.outputGraphlets:
            print(gr[0])
            print(gr[1])
            print(gr[2])
            print("\r\n")
            G.add_edge(gr[0], gr[2])
            edge_labels[(gr[0], gr[2])] = gr[1]
            cCounter=cCounter+1     
        #draw graph
        if self.drawGraphFlag:
            pos = nx.spring_layout(G)
            nx.draw(G, pos)
            nx.draw_networkx_labels(G, pos)
            nx.draw_networkx_edge_labels(G, pos, edge_labels)
            plt.show()      

    def getConceptText(self, chunk):
        if chunk._.said:
            chunk = chunk._.said
        for chunkItem in self.doc.noun_chunks:
            if chunk == chunkItem.root:
                concept = chunkItem.text
        return concept  
        
    def checkConj(self, chunk1):      
        #contains rules to extract concepts and relations
        #examine each item between first and last element in the path   
        rowInd=0
        simAvgRowAll = []
        simForIndex = []
        try:
            for conjPath in self.df.loc[:, chunk1]:  
                if conjPath and not isinstance(conjPath, float):
                    row = self.df.iloc[rowInd]
                    sim = []
                    for i, val in enumerate(row):
                        if val == conjPath:
                            colName =self.df.columns[i]
                            sim.append(chunk1.similarity(colName))
                    if len(sim[:-1]) > 0:
                        simAvgRow = sum(sim[:-1]) / len(sim[:-1])
                        simAvgRowAll.append(simAvgRow)
                else:
                    simAvgRow = 0
                simForIndex.append(simAvgRow)
                rowInd = rowInd+1
            maxIndex = simForIndex.index(max(simAvgRowAll))
            conjChunk = list(self.df.index)[maxIndex]
        except:
            conjChunk = chunk1
        return conjChunk                 
        
def main():

    nlp = spacy.load('en_core_web_sm')
    #sentence = (u"I like strawberries")
    #sentence = (u"The engine comprises a black row and a beam resting on said row.")
    #sentence=(u"The booster engine includes always a first compressor blade row1 attached to the first drive shaft and a second compressor blade row attached to  the second drive shaft.")
    #sentence=(u"Very important engine components in modern automobile engines are the blade, the flywheel, the crankshaft and the piston.")
    #sentence=(u"The engineer and the auditor tested the blade, the flywheel, the crankshaft and the piston.")
    sentence = (u"The gas turbine engine comprises the unitary gas generator and a counterrotatable power turbine connected to the unitary gas generator.")
    #sentence = (u"A gas turbine engine comprising a unitary gas generator effective for generating both combustion gases and nitrogen dioxides.")
    #sentence = (u"A gas turbine engine includes a unitary gas generator effective for generating combustion gases and a counterrotatable power turbine connected to a shaft.")
    #sentence = (u"The invention relates to a variable cycle gas turbine engine of the bypass type and, more particularly, to a variable cycle gas turbine engine suitable for powering a supersonic aircraft wherein the engine bypass ratio and gas flow may be controlled to satisfy particular engine operating conditions. ")
    #sentence = (u"The basic operation of the gas turbine is a Brayton cycle with air as the working fluid.")
    #sentence = (u"a gas turbine engine holds groups of blades.")
    sentence = (u"Suitable retaining elements or vane lugs 124 and 126 may be provided, for example, towards the upstream edge and downstream edge of the outer ring 112 (see ), for engagement with corresponding retaining elements or case slots 124′, 126′, on the inner side of the outer case 30. Referring to , mid turbine frame 28 is shown again, but in this view an upstream turbine stage which is part of the high pressure turbine assembly 24 of , comprising a turbine rotor (not numbered) having a disc 200 and turbine blade array 202, is shown, and also shown is a portion of the low pressure turbine case 204 connected to a downstream side of MTF 28 (fasteners shown but not numbered). The turbine disc 200 is mounted to the turbine shaft 20 of .")
    doc = nlp(sentence)
    
    #options = {'compact': True, 'color': 'blue'}
    #displacy.serve(doc, style='dep', options=options)
    
    for chunk in doc.noun_chunks:
        print(chunk.text, chunk.root.text, chunk.root.dep_,
            chunk.root.head.text, [left for left in chunk.root.lefts], [right for right in chunk.root.rights], [ancestor for ancestor in chunk.root.ancestors], [child for child in chunk.root.children])
    '''    
    print("\r\n")
    for token in doc:
        print(token.text, token.dep_, token.head.text, token.right_edge, 
            [left for left in token.lefts], [right for right in token.rights], [child for child in token.children], [ancestor for ancestor in token.ancestors])
     '''   
    drawGraph = True
    extractGraphlets(sentence, drawGraph) 

if __name__ == "__main__":
    main()


