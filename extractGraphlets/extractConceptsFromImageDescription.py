#!/usr/bin/env python3.5
# coding: utf8
'''
06092018:  extract all noun chunks before image reference labels (e.g. 'gas turbine engine' in "....gas turbine engine 20 in fig 1".)

'''

import spacy
from spacy.tokens import Token
from spacy import displacy
from extractGraphlets_v5 import extractGraphlets

class conFromImDesc():
    def __init__(self, textFile):
        nlp = spacy.load('en_core_web_sm')
        self.fileName = textFile
        self.text = self.readFile()
        self.doc = nlp(self.text)
        self.getNounChunks()
        self.reparseNounChunks()
        extractGraphlets(self.doc, drawGraphFlag=True)
        #numChunks = self.findNums()
        test='a'

    def readFile(self):
        with open(self.fileName, 'r') as in_file:
                text = in_file.read()
                return text

    def getNounChunks(self):
        for chunk in self.doc.noun_chunks:
            print(chunk.text, chunk.root.text, chunk.root.dep_,
                chunk.root.head.text, [left for left in chunk.root.lefts], [right for right in chunk.root.rights], [ancestor for ancestor in chunk.root.ancestors], [child for child in chunk.root.children])

    def reparseNounChunks(self):
        for noun_phrase in list(self.doc.noun_chunks):
            noun_phrase.merge(noun_phrase.root.tag_, noun_phrase.root.lemma_, noun_phrase.root.ent_type_)

        print("\r\n")
        #for token in self.doc:
         #   print(token.text, token.dep_, token.pos_, token.head.text, token.right_edge, 
          #      [left for left in token.lefts], [right for right in token.rights], [child for child in token.children], [ancestor for ancestor in token.ancestors])

        tes ='a'

    def findNums(self):
        numChunks = []
        for token in self.doc:
            if token.pos_ == 'NOUN' and token.nbor().pos_ == 'NUM':
                print([token, token.nbor()])
                numChunks.append(token)
        return numChunks

def main():
        textFile = '/home/rupa/patentInvalidation/development/DataKM/drawingDescription/example.txt'
        conFromImDesc(textFile)
        

if __name__ == "__main__":
    main()
