#!/usr/bin/env python3.5
# coding: utf8

'''
240818: given a test, extracts all sentences that contain the keyword."

'''

import nltk
from extractGraphlets_v5 import extractGraphlets
import s[ac]

class sentsByKeyword():
    def __init__(self, textFile, keyword, getAll):
        self.textFile = textFile
        self.keyword = keyword
        self.sents = self.readFile()
        if getAll:
            self.extractAll()
        else:
            self.extractByKeyword()

    def readFile(self):
        with open(self.textFile, 'r') as in_file:
            text = in_file.read()
            sents = nltk.sent_tokenize(text)
            return sents

    def extractAll(self):
        for sent in self.sents:
            print(sent)
            #extractGraphlets(sent, drawGraphFlag = False) 
            print("\r\n")
       

    def extractByKeyword(self):
        for sent in self.sents:
            if self.keyword in sent:
                print(sent)
                extractGraphlets(sent, drawGraphFlag = False) 
                print("\r\n")


def main():
        textFile = '/home/rupa/patentInvalidation/development/DataKM/xmlPatentsGasTurbine/482'
        keyword = "comprise"
        getAll = True
        sentsByKeyword(textFile, keyword, getAll)

if __name__ == "__main__":
    main()