#!/usr/bin/env python3.5
# coding: utf8

'''
240818: given a folder, extracts all files that contain the keyword."

'''

import os
import mmap
from shutil import copyfile

c=0
for root, dirs, files in os.walk("/home/rupa/ROSA/graphlets", topdown=False):
    for name in files:
        fileName = os.path.join(root, name)
        #print(fileName)
        searchString = ['turbofan', 'gearbox']
        f = open(fileName, 'r')
        a =f.read()
        for word in searchString:
            if 'energy storage' in a and 'aircraft' in a:
                print(fileName)
                dst = os.path.join('/home/rupa/RosaEnergyStorage_aircraft/', name)
                copyfile(fileName, dst)
                c=c+1
                print(c)
print(c)
   
