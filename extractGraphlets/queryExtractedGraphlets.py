#!/usr/bin/env python3.5
# coding: utf8
'''
110908: calls extractGraphlets_v5 to get graphlets from all sentences in a textfile.

13/09/18: v2: includes GUI

'''

import sys
import os
import networkx as nx
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import interactive
import tkinter as tk
from tkinter import *
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure
from tkinterFns import drawGUI
import numpy as np
import re


class queryExtractedGraphlets():
    def __init__(self, dirName):
        self.dir = dirName
        self.readFiles()
        #self.graphlets = extractGraphlets
        #self.text = self.readFile()
        #self.spacyText = nlp(self.text)
        #self.graphlets = self.sendSents()
        #self.graphlets1 = self.removeDet()
        #self.G, self.edge_labels = self.drawGraph()
        #self.genGUI()

    def genGUI(self):
        master=Tk()
        guiObj = drawGUI(master, self.graphlets1)
        # draw networkx plot
        fig = plt.figure(figsize=(5,4))  
        pos = nx.spring_layout(self.G, k=0.5*1/np.sqrt(len(self.G.nodes())))
        nx.draw(self.G, pos, node_size = 100)
        nx.draw_networkx_labels(self.G, pos, font_size = 10)
        nx.draw_networkx_edge_labels(self.G, pos, self.edge_labels, font_size = 10)
        guiObj.displayFig(fig)
        #get query
        #print(guiObj.text)
        #queryTerm = guiObj.text
        #print(queryTerm)
        master.mainloop()

    def readFiles(self):
        for root, dirs, files in os.walk(self.dir, topdown=False):
            for name in files:
                fileName = os.path.join(root, name)
                with open(fileName, 'r') as in_file:
                    dictList = self._getGraphlets(in_file)

    def _getGraphlets(self, in_file):
        dictList=[]
        grDict={}
        for line in in_file:
            if line.startswith("SENTENCE"):
                sLine = line.split('=')[1]
            if line.startswith("RELATION"):
                line1 = line.split('-->')
                grDict['e1']= line1[0].split('=')[1].strip()
                grDict['e2'] = line1[2].split('> ')[1].strip()
                grDict['rel'] = line1[1].split('> ')[1].strip()
                line2 = line1[3].split('--==--')
                grDict['patentID'] = line2[0].split('=')[1].strip()
                grDict['sentenceNo'] = line2[1].split('=')[1].strip()
                grDict['org'] = line2[2].split('=')[1].strip()
                grDict['date'] = line2[3].split('=')[1].strip()
                grDict['sentence'] = sLine.strip()
                dictList.append(grDict)
                grDict={}
        return dictList
                

        a='test'            

            



    def sendSents(self):
        graphlets = []
        for count, sent in enumerate(self.spacyText.sents):
            #print("Sentence {}".format(count))
            #print(sent.string.strip())
            exGrObj = extractGraphlets(sent.string.strip(),drawGraphFlag=False)
            graphlets.extend(exGrObj.outputGraphlets)
        #print("\r\n")
        graphletsT=[]
        for gr in graphlets: # get unique graphlets
            graphletsT.append(tuple(gr))
        return list(set(graphletsT))
       
    def removeDet(self):
        removal_list = ["the", "a", "an", "The", "A", "An"]
        graphlets1 = []
        for gr in self.graphlets:  
            gr1=[]
            for entity in gr: 
                edit_string_as_list = entity.split()
                final_list = [word for word in edit_string_as_list if word not in removal_list]
                final_string = ' '.join(final_list)
                gr1.append(final_string)
            graphlets1.append(gr1)
        return graphlets1

    def drawGraph(self):
        G = nx.DiGraph()
        edge_labels={}
        for gr in self.graphlets1:
            #print(gr[0])
            #print(gr[1])
            #print(gr[2])
            #print("\r\n")
            G.add_edge(gr[0], gr[2])
            edge_labels[(gr[0], gr[2])] = gr[1]
        return G, edge_labels

    def _getSubNodes(self, query):
        subNodes = [gr for gr in self.graphlets1 if any(x in query for x in gr)]
        #print("\r\n")
        #print("the query graphlets are:")
        #print("\r\n")
        #print(subNodes)
        if subNodes:
            SG = nx.DiGraph()
            edge_labels={}
            for sn in subNodes:
                SG.add_edge(sn[0], sn[2])
                edge_labels[(sn[0], sn[2])] = sn[1]
            pos = nx.circular_layout(SG)
            nx.draw(SG, pos)
            nx.draw_networkx_labels(SG, pos)
            nx.draw_networkx_edge_labels(SG, pos, edge_labels)
            plt.figure()
            plt.show(block=False) 
            
def main():
        dirName = '/home/rupa/generateGraphletTree'
        queryExtractedGraphlets(dirName)
        

if __name__ == "__main__":
    main()
