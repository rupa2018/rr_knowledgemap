#!/usr/bin/env python3.5
# coding: utf8

''' extracts graphlets from sentence using spacy's noun_chunks and dependency parsing


    v1: 08/08/18: uses networkx to find shortest path between two terms

''' 
import spacy
import networkx as nx
from spacy.symbols import nsubj, VERB
from spacy.tokens import Token
from spacy import displacy
import numpy as np
import itertools as it
import matplotlib.pyplot as plt
import sys
import os

print(sys.version_info)
nlp = spacy.load('en_core_web_sm')

#sentence = (u"I like strawberries")
#sentence = (u"The engine comprising a black row and a beam resting on said row.")
#sentence=(u"The booster engine includes always a first compressor blade row attached to the first drive shaft and a second compressor blade row attached to the second drive shaft.")
#sentence=(u"Very important engine components in modern automobile engines are the blade, the flywheel, the crankshaft and the piston.")
#sentence=(u"Jack and Jill tested the blade, the flywheel, the crankshaft and the piston.")
#sentence = (u"A gas turbine engine comprises a unitary gas generator and a counterrotatable power turbine connected to said gas generator.")
#sentence = (u"A gas turbine engine comprising a unitary gas generator effective for both generating combustion gases and nitrogen dioxides.")
sentence = (u"A gas turbine engine includes a unitary gas generator effective for generating combustion gases and a counterrotatable power turbine connected to a shaft.")
doc = nlp(sentence)
options = {'compact': True, 'color': 'blue'}
#displacy.serve(doc, style='dep', options=options)

for chunk in doc.noun_chunks:
    print(chunk.text, chunk.root.text, chunk.root.dep_,
          chunk.root.head.text, [left for left in chunk.root.lefts], [right for right in chunk.root.rights], [ancestor for ancestor in chunk.root.ancestors], [child for child in chunk.root.children])
    
print("\r\n")
for token in doc:
    print(token.text, token.dep_, token.head.text, token.right_edge, 
          [left for left in token.lefts], [right for right in token.rights], [child for child in token.children], [ancestor for ancestor in token.ancestors])
    if token.dep_ == 'conj':
       for chunk in doc.noun_chunks:
           print(token.text, chunk.root.text, token.similarity(chunk.root))
  
class extractGraphlets():

    def __init__(self, inputSentence):
        nlp = spacy.load('en_core_web_lg')
        self.sentence = inputSentence
        self.doc = nlp(self.sentence)
        Token.set_extension('has_said', default=False)
        Token.set_extension('said', default = None)
        self.setTokenExtensions()
        self.graph = self.getNetworkxGraph()
        self.chunkCombs = self.getChunkCombs()
        self.getGraphlets()

    def findConjChunk1(self, chunk1):
            chunkSim = []
            if chunk1.dep_ == 'conj':
                #find most similar chunk on its left
                for chunk2 in self.doc.noun_chunks:
                    chunkSimInd = chunk1.similarity(chunk2.root)
                    if chunkSimInd == 1:
                        break
                    else:
                        chunkSim.append(chunkSimInd)
                #find max sim
            maxIndex = list.index(max(chunkSim))
            conjChunk = self.doc.noun_chunks[maxIndex]
            #find its root and then its left
            conjMatch = conjChunk.root.head.lefts 
            nextWord = conjMatch
            doWhile = True          
            while doWhile and nextWord.root.i < nextWord.i and nextWord:
                nextWord = self.findHead(nextWord)
            return nextWord

    def findSimConj(self, conjGraphlets):
        chunkSim = []
        nextWord = chunkRoot
        doWhile = True       
        allWordSim = []   
        while nextWord and nextWord.i < nextWord.head.i:
            nextWord = self.findHead(nextWord)
            if nextWord in [chunk.root for chunk in self.doc.noun_chunks]:
                sim = chunkRoot.similarity(nextWord)
                allWordSim.append([nextWord, sim])
        maxSim = max([x[1] for x in allWordSim])
        maxSimIndex = allWordSim.index(maxSim) # most similar chunkroot in path
        startWord = allWordSim[maxSimIndex][0]
        doWhile = True
        while doWhile or startWord not in self.doc.noun_chunks:
            nextWordPath = self.findHead(startWord)



             
    def findHead(self, startWord):
            #stop
        if startWord.dep_ == 'ROOT':
            for word in startWord.lefts:
                if word in [chunk for chunk in self.doc.noun_chunks]:
                    nextWord = word 
        else:
            nextWord = startWord.root
        return nextWord

    def setTokenExtensions(self):
        for token in self.doc:
            if token.text == "said":
                for chunk in self.doc.noun_chunks:
                    if token.nbor() in chunk:
                        saidRoot = chunk.root 
                        reqWords = chunk.text.split()
                        try:
                            reqWords.remove("said") # chunk with.split()d" e.g. "gas generator" instead of "said gas generator"
                        except ValueError:
                            pass
                    #find most similar 
                        c=0
                        relevantChunks = []
                        for numWords in range(len(reqWords),0,-1):
                            partReqWords = reqWords[-numWords:] 
                            for chunk in self.doc.noun_chunks:
                                if token.i > chunk.root.i:
                                    if ' '.join(partReqWords) in chunk.text:
                                        relevantChunks.append(chunk)
                                else:
                                    break
                            if relevantChunks:
                                break
                            c=c+1   
                        if relevantChunks:    
                            saidChunk = relevantChunks[-1].root
                            saidRoot._.set('said', saidChunk) 
                            
    def getNetworkxGraph(self):
        edges = []        
        for token in self.doc:         
            if token._.said:
                token = token._.said
            if token.dep_ == 'nsubj':
                edges.append((token, token.head))
            if token.rights:
                for right in token.rights:
                    if right._.said:
                        right = right._.said
                    edges.append((token, right))
            if token.head.dep_ == 'nsubj':
                for ancestor in token.ancestors:
                    edges.append((token, ancestor))
        graph = nx.DiGraph(edges)
        return graph

    def getChunkCombs(self):
        #first get noun_chunks, then extract root and root position of each chunk
        chunkRootIndexList=[]
        for chunk in self.doc.noun_chunks:
            chunkRootIndex = chunk.root
            chunkRootIndexList.append(chunkRootIndex)
        # get all combinations of chunkRootIndexList
        chunkCombs = list(it.combinations(chunkRootIndexList, 2))    
        return chunkCombs

    def getGraphlets(self):
        #get shortest dependecy path
        graphlets=[]
        for chunkComb in self.chunkCombs:
            try:
                print(chunkComb)
                c=0
                chunkCombList=[]
                for chunk in chunkComb:
                    if chunk._.said:
                        chunkCombList.append(chunk._.said)
                    else:
                        chunkCombList.append(chunk)
                    c=c+1
                shPath=(nx.shortest_path(self.graph, source=chunkCombList[0], target=chunkCombList[1]))
                #evaluate each dependency path to get entity and graphlets               
                shPathEv = self.evaluateShPath(shPath, chunkCombList)
                if shPathEv:   
                    graphlet1 = self.generateOutput(shPathEv, chunkCombList)
                    if graphlet1:
                        graphlets.append(graphlet1)                 
            except Exception as e:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                #print(exc_type, fname, exc_tb.tb_lineno)
        if graphlets:  
            #draw graph
            #find conjunctions
            grIndex=0
            for gr in graphlets:
                if gr[2].dep_ =='conj':
                    conjGraphlets.append(graphlets[grIndex])
                grIndex=grIndex+1
            if len(conjGraphlets > 1):
                self.findSimConj(conjGraphlets)

            self.drawGraph(graphlets)

    def drawGraph(self, graphlet):
        G = nx.DiGraph()
        cCounter=0
        edge_labels={}
        for gr in graphlet:
            G.add_edge(gr[0], gr[2])
            relationStr = ' '.join([x.text for x in gr[1]])
            edge_labels[(gr[0], gr[2])] = relationStr
            cCounter=cCounter+1       
        #draw graph
        pos = nx.spring_layout(G)
        nx.draw(G, pos)
        nx.draw_networkx_labels(G, pos)
        nx.draw_networkx_edge_labels(G, pos, edge_labels)
        plt.show()      
            
    def generateOutput(self, shPathEv, chunkComb):
        concept1 = self.getConceptText(chunkComb[0])
        concept2 = self.getConceptText(chunkComb[1])
        relation = shPathEv[1:-1]
        if concept1 and relation and concept2:
            print(concept1)
            print(' '.join([x.text for x in relation]))
            print (concept2)
            print("\r\n")
            return concept1, relation, concept2

    def getConceptText(self, chunk):
        for chunkItem in self.doc.noun_chunks:
            if chunk == chunkItem.root:
                concept = chunkItem.text
        return concept      
        
    def evaluateShPath(self, shPath, chunkComb):      
        #contains rules to extract concepts and relations
        #examine each item between first and last element in the path
        print([word for word in shPath])
        if len(shPath) > 2:
            shPath1 = self.getShPath(shPath, chunkComb)
            return shPath1

    def getShPath(self, shPath, chunkComb):
        if any(x in shPath[1:-1] for x in [chunk.root for chunk in self.doc.noun_chunks]):
            #find lists          
            elemPrev = ''
            elemCounter =0
            shPath2 = [chunkComb[0]]
            for elem in shPath:
                elemCounter = elemCounter +1
                if elem == chunkComb[1] and elemPrev in [chunk.root for chunk in self.doc.noun_chunks]:
                    for item in shPath[1:-1]:
                        if item not in [chunk.root for chunk in self.doc.noun_chunks]:
                            shPath2.append(item)
                        else:
                            break
                elemPrev = elem
            if shPath2:
                shPath2.append(chunkComb[1])
                shPath1 = shPath2
        else:
            shPath1 = shPath     
        return shPath1    
        
def main():
        extractGraphlets(sentence) 

if __name__ == "__main__":
    main()


