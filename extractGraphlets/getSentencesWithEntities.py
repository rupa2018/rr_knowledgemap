#!/usr/bin/env python3.5
# coding: utf8

'''
240818: given a test, extracts all sentences that contain the keyword."

'''

import nltk
from extractGraphlets_v7 import extractGraphlets
import glob
import spacy
from spacy.tokens import Token
import re

class sentsByEnts():
    def __init__(self):
        self.nlp = spacy.load('en_core_web_sm')
        self.fileNames =  glob.glob('/home/rupa/patentInvalidation/development/DataKM/xmlPatentsGasTurbine/' + '*')
        self.entityListFile = '/home/rupa/patentInvalidation/development/DataKM/wordListSimple.txt'
        self.entityList = self.getEntities() 
        self.getSents()

    def getEntities(self):
        with open(self.entityListFile,'r') as entityListF:
            entityList = entityListF.read().split('\n')
            return entityList
            
    def getSents(self):
        c=0
        for fileName in self.fileNames:
            with open(fileName, 'r') as in_file:
                text = in_file.read()
            tokens = self.nlp(text)
            for sent in tokens.sents:
                sent1 = sent.string.strip()
                if sent1[0].isupper():
                    wordList = []
                    for word in self.entityList[:-1]:
                        if re.search(r"\b" + re.escape(word) + r"\b", sent1, re.IGNORECASE):
                            wordList.append(word)
                    
                    if len(wordList) > 1:
                        gObject = extractGraphlets(sent1, wordList, drawGraphFlag = False) 
                        graphlets = gObject.outputGraphlets

                        if graphlets:
                            c=c+1
                            print(wordList)
                            print(sent)
                            print(graphlets)
                            print(c)
                            print("\r\n")
                        test='a'
                    
                    '''
                    words = [word for word in self.entityList[:-1] if word in sent1]
                    if len(words) > 2:
                        print(words)
                        print(sent)
                        test='a'
            
                    '''
            
            '''
            sents = nltk.sent_tokenize(text)
            for sent in sents:
                words = [word for word in self.entityList[:-1] if word in sent]
                if words:
                    print(sent)
                    print(words)
            '''



'''
    def readFile(self, fileName):
        with open(fileName, 'r') as in_file:
            text = in_file.read()
            return self.text

    def extractAll(self):
        for sent in self.sents:
            if
            print(sent)
            #extractGraphlets(sent, drawGraphFlag = False) 
            print("\r\n")
       

    def extractByKeyword(self):
        for sent in self.sents:
            if self.keyword in sent:
                print(sent)
                extractGraphlets(sent, drawGraphFlag = False) 
                print("\r\n")

'''
def main():
        textFile = '/home/rupa/patentInvalidation/development/DataKM/xmlPatentsGasTurbine/482'
        keyword = "comprise"
        getAll = True
        sentsByEnts()

if __name__ == "__main__":
    main()