#!/usr/bin/env python3.5
# coding: utf8
'''
110908: calls extractGraphlets_v5 to get graphlets from all sentences in a textfile.

'''

import spacy
from spacy.tokens import Token
from spacy import displacy
from extractGraphlets_v5 import extractGraphlets
import networkx as nx
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import interactive


class graphletsFromText():
    def __init__(self, textFile, query=''):
        nlp = spacy.load('en_core_web_sm')
        self.fileName = textFile
        self.text = self.readFile()
        self.spacyText = nlp(self.text)
        self.graphlets = self.sendSents()
        self.graphlets1 = self.removeDet()
        self.drawGraph()
        self.userQuery()
        #numChunks = self.findNums()
        test='a'

    def readFile(self):
        with open(self.fileName, 'r') as in_file:
                text = in_file.read()
                return text

    def sendSents(self):
        graphlets = []
        for count, sent in enumerate(self.spacyText.sents):
            print("Sentence {}".format(count))
            print(sent.string.strip())
            exGrObj = extractGraphlets(sent.string.strip(),drawGraphFlag=False)
            graphlets.extend(exGrObj.outputGraphlets)
        print("\r\n")
        return graphlets
        #for token in self.doc:
         #   print(token.text, token.dep_, token.pos_, token.head.text, token.right_edge, 
          #      [left for left in token.lefts], [right for right in token.rights], [child for child in token.children], [ancestor for ancestor in token.ancestors])
    
    def removeDet(self):
        removal_list = ["the", "a", "an", "The", "A", "An"]
        graphlets1 = []
        for gr in self.graphlets:  
            gr1=[]
            for entity in gr: 
                edit_string_as_list = entity.split()
                final_list = [word for word in edit_string_as_list if word not in removal_list]
                final_string = ' '.join(final_list)
                gr1.append(final_string)
            graphlets1.append(gr1)
        return graphlets1

    def drawGraph(self):
        G = nx.DiGraph()
        edge_labels={}
        for gr in self.graphlets1:
            print(gr[0])
            print(gr[1])
            print(gr[2])
            print("\r\n")
            G.add_edge(gr[0], gr[2])
            edge_labels[(gr[0], gr[2])] = gr[1]   
        #draw graph
        pos = nx.spring_layout(G)
        nx.draw(G, pos)
        nx.draw_networkx_labels(G, pos)
        nx.draw_networkx_edge_labels(G, pos, edge_labels)
        print(plt.get_backend())
        plt.figure(1)
        plt.show(block=False) 

    def userQuery(self):
        contQuery = True
        c=2
        while contQuery:
            query = input("Please enter the query term: ")
            if query:
                plt.figure(c)
                self._getSubNodes(query)
                c=c+1
            else:
                contQuery = False

    def _getSubNodes(self, query):
        subNodes = [gr for gr in self.graphlets1 if any(x in query for x in gr)]
        print("\r\n")
        print("the query graphlets are:")
        print("\r\n")
        print(subNodes)
        if subNodes:
            SG = nx.DiGraph()
            edge_labels={}
            for sn in subNodes:
                SG.add_edge(sn[0], sn[2])
                edge_labels[(sn[0], sn[2])] = sn[1]
            pos = nx.spring_layout(SG)
            nx.draw(SG, pos)
            nx.draw_networkx_labels(SG, pos)
            nx.draw_networkx_edge_labels(SG, pos, edge_labels)
            plt.figure()
            plt.show(block=False) 
            
def main():
        textFile = '/home/rupa/patentInvalidation/development/DataKM/drawingDescription/example.txt'
        graphletsFromText(textFile, query = 'shaft')
        

if __name__ == "__main__":
    main()
