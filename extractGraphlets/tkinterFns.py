#!/usr/bin/env python3.5
# coding: utf8

from tkinter import *
import networkx as nx
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import interactive
import tkinter as tk
from tkinter import *
from tkinter.font import Font
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure
import numpy as np

try:
    import pygraphviz
    from networkx.drawing.nx_agraph import graphviz_layout
except ImportError:
    try:
        import pydotplus
        from networkx.drawing.nx_pydot import graphviz_layout
    except ImportError:
        raise ImportError("This example needs Graphviz and either "
                              "PyGraphviz or PyDotPlus")

class drawGUI():

    text=''
    def __init__(self, master, graphlets):
        self.graphlets = graphlets
        ## config master
        self.master = master
        self.master.title("Knowledge Map")
        self.master.geometry('{}x{}'.format(1700, 1000))
        self.master.grid_columnconfigure(0, weight=1, uniform='third')
        self.master.grid_columnconfigure(1, weight=1, uniform='third')
        self.master.grid_rowconfigure(0, weight=3, uniform='third')
        self.master.grid_rowconfigure(2, weight=2, uniform='third')

        self.fAll = Frame(self.master) # frame for all graphlets
        self.fQu = Frame(self.master) # frame for queried graphlets
        self.fButtons =  Frame(self.master, bg = 'grey')
        self.fGraphAll = Frame(self.master, bg = 'white') # frame for all plot of all graphlets
        self.fGraphQu = Frame(self.master,  bg = 'white')  # frame for all plot of queried graphlets
        self.fQuit = Frame(self.master,  bg = 'grey')
        ## frame layout
        self.fAll.grid(row=0, column=0, sticky=(N, S, W) )
        self.fQu.grid(row=2, column=0, sticky=(N, S, W))
        self.fGraphAll.grid(row=0, column=1, sticky=(N, S, E, W)) 
        self.fGraphQu.grid(row=2, column=1, sticky=(N, S, E, W))
        self.fButtons.grid(row=1, column=0,  sticky=(N, S, E, W))
        self.fQuit.grid(row=1, column=1, sticky=(N, S, E, W))
        ## add text to display all graphlets 
        self.myFont = Font(family="Helvetica", size=14)
        self.tAll = Text(self.fAll)
        self.tAll.configure(font=self.myFont)
        ## scroll bar
        self.sAll = Scrollbar(self.fAll, orient=VERTICAL)
        self.tAll.config(yscrollcommand=self.sAll.set)
        self.sAll.config(command=self.tAll.yview)
        ## graphlets text layout with scroll bar
        self.tAll.grid(row=0, column=0, sticky=(N, S, E, W))
        self.sAll.grid(row=0, column=1, sticky = (N,S))
        self.fAll.grid_rowconfigure(0, weight=1)
        ## display text
        self.displayText(self.tAll,self.graphlets, deleteText = False)

        ## add all widgets to button frame
        ## display query text
        self.lQu = Label(self.fButtons, text="Search query") 
        self.lQu.configure(font=self.myFont) 
        ## get user query
        self.eQu = Entry(self.fButtons, width = 40)
        self.eQu.insert(END, 'Enter search query')
        self.eQu.configure(font=self.myFont)
        ## search buttons
        self.bAny = Button(self.fButtons, text = "Search any", command = self.show_entry_fields)
        self.bAny.configure(font=self.myFont)
        self.bAll = Button(self.fButtons, text = "Search all", command = self.show_entry_fields_all)
        self.bAll.configure(font=self.myFont)
        ## quit program
        self.bQuit = Button(self.fQuit, text="Quit", command=self.endProgram)
        self.bQuit.configure(font=self.myFont)
        ## clear graph & text
        self.bClr = Button(self.fButtons, text="Clear", command=self.clrGraph)
        self.bClr.configure(font=self.myFont)
        ## button frame layout
        self.lQu.grid(row=0, column=0)
        self.eQu.grid(row=0, column=1, columnspan = 3)
        #self.bAny.grid(row=0, column=4)
        self.bAll.grid(row=0, column=5)
        self.bQuit.grid(row=0, column=1, sticky = 'E')
        self.bClr.grid(row=0, column=6)
        ## display number of graphlets
        #self.labelL1 = Label(self.frame, text=str(len(graphlets)))
        #self.labelL1.grid(row=1, column = 5, sticky=(N, S, E, W))
           
        ## display requested graphlets
        self.tQu = Text(self.fQu) 
        self.tQu.configure(font=self.myFont)
        self.sQu = Scrollbar(self.fQu, orient=VERTICAL)
        self.tQu.config(yscrollcommand=self.sQu.set)
        self.sQu.config(command=self.tQu.yview)
        ## query text layout with scroll bar
        self.tQu.grid(row=0, column=0, sticky=(N, S, E, W))
        self.sQu.grid(row=0, column=1, sticky = (N,S))     
        self.fQu.grid_rowconfigure(0, weight=1)   
        
              

    def endProgram(self): # exit GUI and program when user clicks on "quit"
        # top.quit()
        self.master.quit()  

    def clrGraph(self): # clear graph and text based on query
        try:
            self.tQu.delete('0.0',END)
            self.canvas1.get_tk_widget().destroy()
        except:
            pass
    
    def displayText(self, textWidget, graphlets, deleteText): #display graphlets that match query
        try:
            self.tQu.delete('0.0',END)
            self.canvas1.get_tk_widget().destroy()
        except:
            pass
        if graphlets:
            for gr in graphlets:
                print(gr)
                textWidget.insert(END, gr)
                textWidget.insert(END,"\r\n")
        else:
           textWidget.insert(END, "Query terms not found.") 

    def displayFig(self, fig): # draw figure 
        self.canvas = FigureCanvasTkAgg(fig, master = self.fGraphAll)
        #self.canvas.get_tk_widget().grid(row = 0, column =0, sticky="nsew")  
        self.canvas.get_tk_widget().pack(side=TOP, fill=BOTH, expand=1) 

    def show_entry_fields(self): # read entry on button press for any query
        queryTerm = self.eQu.get()
        self._displaySubNodes(queryTerm, 'any')

    def show_entry_fields_all(self): # read entry on button press for all query
        queryTerm = self.eQu.get()
        self._displaySubNodes(queryTerm, 'all')
    
    def _displaySubNodes(self, query, searchType): # get graphlets that match query
        print([word for word in query])
        print(query)
        print (self.graphlets[0])
        if searchType == 'any':
            query1 = query.split(',')
            subNodes = [gr for gr in self.graphlets if any(x in query1 for x in gr)]
        if searchType == 'all':
            query1 = query.split(',')
            print(query1)
            subNodes = [gr for gr in self.graphlets if set(query1).issubset(gr)]
        print(subNodes)            
        self.displayText(self.tQu, subNodes, deleteText = True)
        if subNodes:
            SG = nx.DiGraph()
            edge_labels={}
            for sn in subNodes:
                SG.add_edge(sn[0], sn[2])
                edge_labels[(sn[0], sn[2])] = sn[1]
            figS = plt.figure(figsize=(20,15))  
            pos = graphviz_layout(SG)
            nx.draw(SG, pos, node_size =100)
            nx.draw_networkx_labels(SG, pos, font_size = 10)
            nx.draw_networkx_edge_labels(SG, pos, edge_labels, font_size = 10)
            self.canvas1 = FigureCanvasTkAgg(figS, master = self.fGraphQu)
            self.canvas1.get_tk_widget().pack(side=TOP, fill=BOTH, expand=1) 


def main():
    master=Tk()
    guiObj = drawGUI(master,'graphlet')

    guiObj.displayText("Hello graphlet")
    # draw networkx plot
    G = nx.DiGraph()
    fig = plt.figure(figsize=(5,4))
    G.add_edge(1, 2)   
    pos = nx.spring_layout(G)
    nx.draw(G, pos)
    nx.draw_networkx_labels(G, pos)
    guiObj.displayFig(fig)
    #get query
    print(guiObj.text)
    #queryTerm = guiObj.text
    #print(queryTerm)

    master.mainloop()

        
if __name__ == "__main__":
    main()


