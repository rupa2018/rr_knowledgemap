#!/usr/bin/env python3.5
# coding: utf8

''' extracts graphlets from sentence using spacy's noun_chunks and dependency parsing


    v1: 08/08/18: uses networkx to find shortest path between two terms
    v5: working version. Always make sure that this version works
    11/09/18: Added reparseNounChunks(), which changes the parsed doc so that all words in a noun chunk are contained in the same token

'''
import spacy
import networkx as nx
from spacy.tokens import Token
from spacy import displacy
import numpy as np
import pandas as pd
import itertools as it
import matplotlib.pyplot as plt
import sys
import os


print(sys.version_info)

class extractGraphlets():

    def __init__(self, inputSentence, drawGraphFlag=False):
        nlp = spacy.load('/home/rupa/patentInvalidation/development/DataKM/spacyTrain')
        #nlp = spacy.load('en_core_web_sm')
        self.drawGraphFlag = drawGraphFlag
        self.doc = nlp(inputSentence) #input sentence
        self.allRels = self.reparseNounChunks()
        self.grDict=[]
        self.getNGraphlets()
        #self.reparseRelations()
        #Token.set_extension('said', default = None, force=True)
        #self.setTokenExtensions()
        #self.graph = self.getNetworkxGraph()
        #self.chunkCombs = self.getChunkCombs()
        #self.df = self.getGraphlets()
        #print(self.df.to_string())
        #self.outputGraphlets = self.getOutputGraphlets()
        #print(self.outputGraphlets)
       # print(self.df.to_string())
        #if self.outputGraphlets:
         #   self.drawGraph()
        #self.getNestedGraphlets()      

    def reparseNounChunks(self):
        for chunk in self.doc.noun_chunks:
            print(chunk.text, chunk.root.text, chunk.root.dep_,
                chunk.root.head.text, [left for left in chunk.root.lefts], [right for right in chunk.root.rights], [ancestor for ancestor in chunk.root.ancestors], [child for child in chunk.root.children])

        for noun_phrase in list(self.doc.noun_chunks):
            noun_phrase.merge(noun_phrase.root.tag_, noun_phrase.root.lemma_, noun_phrase.root.ent_type_)
        print("\r\n")
        for token in self.doc:
            print(token.text,token.dep_, token.head.text,
                [left for left in token.lefts], [right for right in token.rights], [child for child in token.children], [ancestor for ancestor in token.ancestors])
        relSpan = []
        allRels = []
        for token in self.doc:  
            if token.text not in [chunk.text for chunk in self.doc.noun_chunks]:
                relSpan.append(token.i)
            else:
                if relSpan:
                   span = self.doc[relSpan[0]:(relSpan[-1]+1)]
                   allRels.append(span)
                   print(span)
                   #span.merge()
                   relSpan = []  
            if token.text == '.':
                break
        for rel in list(allRels):
            rel.merge(rel.root.tag_, rel.root.lemma_, rel.root.ent_type_)       
        
        print("\r\n")
        for token in self.doc:
            print("\r\n")
            print(token.text,token.dep_, token.head.text,
                [left for left in token.lefts], [right for right in token.rights], [child for child in token.children], [ancestor for ancestor in token.ancestors], [tree for tree in token.subtree])       

        return allRels

        options = {'compact': True, 'color': 'blue'}
        #displacy.serve(self.doc, style='dep', options=options)
 
    def getNGraphlets(self):
        self.grDict={}
        for token in self.doc:
            if token.text in [rel.text for rel in self.allRels]:
                if token.dep_ == 'ROOT':
                    ent1 = list(token.children)[0]
                    ent2 = list(token.children)[1]
                    rootToken = token
                    self.grDict[token] = [ent1, rootToken, ent2] 
                    break
        for token in self.doc: 
            if token.text in [rel.text for rel in self.allRels] and token.dep_ != 'ROOT':                
                if token.i < rootToken.i:
                    order = 'preRoot'
                elif token.i > rootToken.i:
                    order = 'postRoot'      
                indGr = [list(token.ancestors)[0], token, list(token.children)[0]]
                self.grDict[token] = indGr 
                for anc in list(token.ancestors)[1::]:
                    #if anc in self.grDict and anc not in indGr:
                     #   l=self._recursively_apply(self.grDict[anc], self._replace_chars, anc, indGr, order)
                     #   print(l)
                    if anc in self.grDict and anc not in indGr:
                        L = self._find_first_element(self.grDict[anc], anc, indGr, order)
                     #   print(l)

    def _replace_chars(self, s, anc, indGr):
        return s.replace(anc, indGr)

    def _recursively_apply(self, l, f, anc, indGr, order):
        try:
            if anc in l:
                if order == 'preRoot':
                    origIndex = l.index(anc)-1
                else:
                    origIndex = l.index(anc)+1
                new = indGr
                l[origIndex] = indGr
        except:
            for n, i in enumerate(l):
                try:
                    if isinstance(i,list):
                        if anc in i:
                            if order == 'preRoot':
                                origIndex = l.index(anc)-1
                            else:
                                origIndex = l.index(anc)+1
                            new = indGr
                            l[origIndex] = indGr 
                    else:
                        if anc.text == i.text:
                            if order == 'preRoot':
                                origIndex = l.index(anc)-1
                            else:
                                origIndex = l.index(anc)+1
                            new = indGr
                            l[origIndex] = indGr                      
                except:
                    l[n] = self._recursively_apply(l[n], f, anc, indGr, order)
        return l

    def _find_first_element(self, L, anc, indGr, order):
        if not L:
            raise ValueError("list is empty")
        item = L[0]
        if type(item) == list:
            try:
                self._find_first_element(L[1:], anc, indGr, order)
            except ValueError:
                # That sublist was empty, maybe the rest of the list
                # contains something...
               print('hello')
        else:
            if anc == item:
                if order == 'preRoot':
                    print('preRoot')
                else:
                    ancIndex = L.index(anc)
                    repIndex = ancIndex + 1
                    L[repIndex] = indGr
                    print('h')
                    return L
            else:
                self._find_first_element(L[1:], anc, indGr, order)
         

    def getNGraphlets2(self):
        self.grDict={}
        for token in self.doc:
            if token.text in [rel.text for rel in self.allRels]:
                if token.dep_ == 'ROOT':
                    self.grDict[token] = {}
                    ent1 = list(token.children)[0]
                    ent2 = list(token.children)[1]
                    rootToken = token
                    #self.grDict[token] = [ent1, rootToken, ent2] 
                    self.grDict[token]['ent1'] = ent1
                    self.grDict[token]['ent2'] = ent2
                    self.grDict[token]['rel'] = rootToken
                    break
        for token in self.doc:
            if token.text in [rel.text for rel in self.allRels] and token.dep_ != 'ROOT':
                self.grDict[token] = {}
                if token.i < rootToken.i:   
                    #get first relationship
                    self.grDict[token]['ent1'] = list(token.ancestors)[0]
                    self.grDict[token]['ent2'] = list(token.children)[0]
                    self.grDict[token]['rel'] = token
                    indGr = [list(token.anc)[0], token, list(token.children)[0]]
                    ancList=[token]
                    for anc in token.ancestors[2::]:
                        if anc in self.grDict and anc not in indGr:
                            self.grDict[anc]

                else:
                    ent2 = self._getEntity(list(token.children)[0])
                    grList = []
                    for anc in token.ancestors:
                        ent1 = self._getEntity(anc)
                        gr = [ent1, token, ent2]
                        grList.append(gr)
                    self.grDict[token]= grList
        rootGr = self.grDict[rootToken]


    def getNGraphlets1(self):
        
        for token in self.doc:
            if token.text in [rel.text for rel in self.allRels]:
                if token.dep_ == 'ROOT':
                    ent1 = list(token.children)[0]
                    ent2 = list(token.children)[1]
                    rootToken = token
                    self.grDict = [ent1, rootToken, ent2] 
                    break
        for token in self.doc:
            if token.text in [rel.text for rel in self.allRels] and token.dep_ != 'ROOT':
                if token.i < rootToken.i:
                    loop1 = True
                    for anc in token.ancestors:
                        if loop1: 
                            indGr = [anc, token, list(token.children)[0]]
                            loop1 = False
                        elif not loop1 and anc in self.grDict and anc not in indGr:
                            #newEnt1 = indGr
                            #newRel = anc
                            #newEnt2 = self.grDict[anc][2]
                            #indGr = [newEnt1, newRel, newEnt2]
                            self.grDict[0] = indGr
                    #self.grDict[token] = [newEnt1, newRel, newEnt2]
                elif token.i > rootToken.i:
                    loop1 = True
                    for anc in token.ancestors:
                        if loop1: 
                            indGr = [anc, token, list(token.children)[0]]
                            loop1 = False
                        elif not loop1 and anc in self.grDict and anc.text not in [ind.text for ind in indGr]:
                            #newEnt1 = self.grDict[anc][0]
                            #newRel = anc
                            #newEnt2 = indGr
                            #indGr = [newEnt1, newRel, newEnt2]
                            self.grDict[2]= indGr
                    #self.grDict[token] = [newEnt1, newRel, newEnt2]
                            
        a='b'

        
        return self.grDict
                
    def _getEntity(self, child):
        if child.text in [chunk.text for chunk in self.doc.noun_chunks]:
            ent = child
        elif child in self.grDict:
            ent = self.grDict[child]
        else:
            ent = None
        return ent

    def setTokenExtensions(self):
        for token in self.doc:
            if token.text == "said":
                for chunk in self.doc.noun_chunks:
                    try:
                        if token.nbor() in chunk:
                            saidRoot = chunk.root 
                            reqWords = chunk.text.split()
                            try:
                                reqWords.remove("said") # chunk with.split()d" e.g. "gas generator" instead of "said gas generator"
                            except ValueError:
                                pass
                    except:
                        pass
                    #find most similar 
                        relevantChunks = []
                        for numWords in range(len(reqWords),0,-1):
                            partReqWords = reqWords[-numWords:]
                            for chunk in self.doc.noun_chunks:
                                if token.i > chunk.root.i:
                                    if ' '.join(partReqWords) in chunk.text:
                                        relevantChunks.append(chunk)
                                else:
                                    break
                            if relevantChunks:
                                break
                        if relevantChunks:
                            saidChunk = relevantChunks[-1].root
                            saidRoot._.set('said', saidChunk)

    def getNetworkxGraph1(self):
        edges = []
        for token in self.doc:
            if token.dep_ == 'nsubj':
                edges.append((token, token.head))
            if token.rights:
                for right in token.rights:
                    edges.append((token, right))
            if 'nsubj' or 'nsubjpass' in token.head.dep_:
                for ancestor in token.ancestors:
                    edges.append((token, ancestor))
        graph = nx.DiGraph(edges)
        return graph

    def getNetworkxGraph(self):
        edges = []
        for token in self.doc:
            for child in token.children:
                edges.append((token, child))
        graph = nx.Graph(edges)
        return graph

    def getChunkCombs(self):
        #first get noun_chunks, then extract root and root position of each chunk
        chunkRootIndexList=[]
        for chunk in self.doc.noun_chunks:
            chunkRootIndexList.append(chunk.root)
        # get all combinations of chunkRootIndexList
        chunkCombs = list(it.combinations(chunkRootIndexList, 2))
        return chunkCombs

    def getShortestPath(self, chunkCombList):
        try:
            shPath=nx.shortest_path(self.graph, source=chunkCombList[0], target=chunkCombList[1])
            return shPath
        except Exception:
            print("no shortest path")
            pass

    def getGraphlets(self):
        #get shortest dependecy path
        indexPD = [chunk.root for chunk in self.doc.noun_chunks]
        noneArray = np.full((len(indexPD), len(indexPD)), None)
        df =pd.DataFrame(data = noneArray, index=indexPD, columns = indexPD)
        for chunkComb in self.chunkCombs:
            shPath = self.getShortestPath(chunkComb)
            if shPath and len(shPath) > 2:
                shPathEv = self.evaluateShPath(shPath[1:-1], chunkComb)
            else:
                shPathEv = []
            df.loc[chunkComb[0], chunkComb[1]]=shPathEv
        return df

    def getOutputGraphlets(self):
        finalGraphlets = []
        for chunkPair in self.chunkCombs:
            if chunkPair[1].dep_ == 'conj':
                concept1T = self.checkConj(chunkPair[1])
                if not self.df.at[concept1T, chunkPair[1]]:
                    relation = []
                else:
                    relation = self.df.loc[concept1T, chunkPair[1]]
                concept1 = self.getConceptText(concept1T)
                concept2 = self.getConceptText(chunkPair[1])
            else:
                concept1 = self.getConceptText(chunkPair[0])
                concept2 = self.getConceptText(chunkPair[1])
                if not self.df.at[chunkPair[0], chunkPair[1]]:
                    relation = []
                else:
                    relation = self.df.loc[chunkPair[0], chunkPair[1]]
            if concept1 and concept2 and relation:
                relationStr = ' '.join([x.text for x in relation])
                finalGraphlets.append([concept1, relationStr, concept2])
        return finalGraphlets

    def evaluateShPath(self, shPath, chunkComb):
        #contains rules to extract concepts and relations
        #check if there's another noun chunk
        shPath1=[]
        print("\r\n")
        print(chunkComb)
        print(shPath)
        if all(chunkComb[0].i < x.i < chunkComb[1].i for x in shPath):
            if any(x in shPath for x in [chunk.root for chunk in self.doc.noun_chunks]):
                #find lists
                if shPath[-1] in [chunk.root for chunk in self.doc.noun_chunks]:
                    for item in shPath:
                        if item not in [chunk.root for chunk in self.doc.noun_chunks]:
                            shPath1.append(item)
                        else:
                            break
            else:
                shPath1 = shPath
            if len(shPath1) > 1 and 'said' not in [word.text for word in shPath1]:
                shPath2 = [shPath1[-1]]
                for count, token in reversed(list(enumerate(shPath1))):
                    if token !=shPath1[0]:
                        leftToken = shPath1[count-1]
                    if leftToken == token.nbor(-1):
                        shPath2.insert(0,leftToken)
                    else:
                        break
            else:
                shPath2 = shPath1
        else:
            shPath2 = []
        print(shPath2)
        return shPath2

    def drawGraph(self):
        G = nx.DiGraph()
        cCounter=0
        edge_labels={}
        print("\r\n")
        for gr in self.outputGraphlets:
            print(gr[0])
            print(gr[1])
            print(gr[2])
            print("\r\n")
            G.add_edge(gr[0], gr[2])
            edge_labels[(gr[0], gr[2])] = gr[1]
            cCounter=cCounter+1
        #draw graph
        if self.drawGraphFlag:
            pos = nx.spring_layout(G)
            nx.draw(G, pos)
            nx.draw_networkx_labels(G, pos)
            nx.draw_networkx_edge_labels(G, pos, edge_labels)
            plt.show()

    def getConceptText(self, chunk):
        if chunk._.said:
            chunk = chunk._.said
        for chunkItem in self.doc.noun_chunks:
            if chunk == chunkItem.root:
                concept = chunkItem.text
        return concept

    def checkConj(self, chunk1):
        #contains rules to extract concepts and relations
        #examine each item between first and last element in the path
        rowInd=0
        simAvgRowAll = []
        simForIndex = []
        try:
            for conjPath in self.df.loc[:, chunk1]:
                if conjPath and not isinstance(conjPath, float):
                    row = self.df.iloc[rowInd]
                    sim = []
                    for i, val in enumerate(row):
                        if val == conjPath:
                            colName =self.df.columns[i]
                            sim.append(chunk1.similarity(colName))
                    if len(sim[:-1]) > 0:
                        simAvgRow = sum(sim[:-1]) / len(sim[:-1])
                        simAvgRowAll.append(simAvgRow)
                else:
                    simAvgRow = 0
                simForIndex.append(simAvgRow)
                rowInd = rowInd+1
            maxIndex = simForIndex.index(max(simAvgRowAll))
            conjChunk = list(self.df.index)[maxIndex]
        except:
            conjChunk = chunk1
        return conjChunk

def main():

    nlp = spacy.load('en_core_web_sm')
    sentence = (u"I like strawberries")
    sentence = (u"The engine comprises a black row and a beam resting on said row.")
    #sentence=(u"The booster engine includes always a first compressor blade row1 attached to the first drive shaft and a second compressor blade row attached to the second drive shaft.")
    #sentence=(u"Very important engine components in modern automobile engines are the blade, the flywheel, the crankshaft and the piston.")
    #sentence=(u"The engineer and the auditor tested the blade, the flywheel, the crankshaft and the piston.")
    #sentence = (u"The gas turbine engine comprises the unitary gas generator and a counterrotatable power turbine connected to the unitary gas generator.")
    #sentence = (u"A gas turbine engine comprising a unitary gas generator effective for generating both combustion gases and nitrogen dioxides.")
    #sentence = (u"A gas turbine engine includes a unitary gas generator effective for generating combustion gases and a counterrotatable power turbine connected to a shaft.")
    #sentence = (u"The invention relates to a variable cycle gas turbine engine of the bypass type and, more particularly, to a variable cycle gas turbine engine suitable for powering a supersonic aircraft wherein the engine bypass ratio and gas flow may be controlled to satisfy particular engine operating conditions. ")
    sentence = (u"The basic operation of the gas turbine is a Brayton cycle with air as the working fluid.")
    #sentence = (u"An MTF system generally includes a bearing housing around a main shaft of the engine and connected to a spoke casing.")
    #sentence = (u"A mid turbine frame system is disposed between the high pressure turbine assembly and the low pressure turbine assembly and supports bearings around the shaft.")
    #sentence = (u"The blades are usually made of Nickel based super alloys for high pressure compressors with operating temperature exceeding 500 Celsius.")
    #sentence = (u"The air is mixed with fuel in the combustor to generate hot combustion gases, which flow downstream to the turbine stages to extract energy.")
    #sentence = (u"I like London and Berlin.")
    doc = nlp(sentence)

    #options = {'compact': True, 'color': 'blue'}
    #displacy.serve(doc, style='dep', options=options)

    for chunk in doc.noun_chunks:
        print(chunk.text, chunk.root.text, chunk.root.dep_,
            chunk.root.head.text, [left for left in chunk.root.lefts], [right for right in chunk.root.rights], [ancestor for ancestor in chunk.root.ancestors], [child for child in chunk.root.children])

    print("\r\n")
    for token in doc:
        print(token.text, token.dep_, token.head.text,
            [left for left in token.lefts], [right for right in token.rights], [child for child in token.children], [ancestor for ancestor in token.ancestors])

    drawGraph = True
    extractGraphlets( sentence, drawGraph)
    # for sentence in :
    #     E._process(sentence)
    # extractGraphlets._process(sentence, drawGraph)
    # extractGraphlets(sentence, drawGraph)

if __name__ == "__main__":
    main()
