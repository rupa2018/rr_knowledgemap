import sys, glob, os
import networkx as nx
import matplotlib.pyplot as plt
import cv2
import random
import numpy as np

class read_graphlets():
    """docstring for read_graphlets."""
    def __init__(self):
        self.dir = "/media/mo/Data4/ROSA/db/graphlets/"
        self.save_dir = "/media/mo/Data4/ROSA/db/gas_turbine/"
        self.folders = sorted([x[0] for x in os.walk(self.dir)])
        self.img_dir = "/media/mo/Data4/ROSA/db/gas_turbine/images/"

    def _update_graphlets(self):
        count1, count2, count3 = 0, 0, 0
        self.lines = []

        # relations = []
        good_relations = ["compris", "includ", "contain", "wherein","having"]
        for folder in self.folders:
            if os.path.isfile(folder+"/finished.data"):
                files = glob.glob(folder+"/*.txt")
                for file_ in files:
                    F = open(file_,"r")
                    for line in F:
                        if "RELATION=" in line:
                            line = line.lower()
                            if "gas turbine" in line:

                                con1, r, con2, meta = line.split(" -->> ")

                                if "gas turbine" in con1:
                                    # if "gas turbine" in con1 and "compressor" in con2:
                                    #     if r not in relations:
                                    #         relations.append(r)

                                    # print line
                                    count1 += 1
                                    for rel in good_relations:
                                        if rel in r:
                                    # if "includ" in line or "contain" in line or "wherein" in line:
                                        # print line
                                            count2 += 1
                                            if line not in self.lines:
                                                self.lines.append(line)
                                                # print line
                                                count3 += 1
                    print "total relations=",count1
                    print "gas turbine relations=",count2
                    print "unique gas turbine relations=", count3
                    F.close()
                    # sys,exit(1)

        F_w = open(self.save_dir+"all_graphlets.txt", "w")
        for line in self.lines:
            F_w.write(line)
        F_w.close()

    def _filter(self, con):
        con = con.replace(",","",-1)
        con = con.replace(";","",-1)
        con = " "+con+" "
        for word1 in self.bad_words:
            con = con.replace(word1," ",-1)

        try:
            while con[0] == " ":
                con = con[1:]
            while con[-1] == " ":
                con = con[:-1]
        except:
            pass

        for name in self.component_names:
            if name in con:
                Name1 = name
                break
            else:
                Name1 = ""

        return con, Name1

    def _read_graphlets(self):
        self.graph = []
        file_ = self.save_dir+"all_graphlets.txt"
        self.bad_words =  [" the ", " a ", " an ", " some ", " many ", " , ", " such ", " typical ", " conventional ", " at "]
        self.bad_words += [" (e.g. ", " any ", " as ", " shown ", " in ", " all ", " almost ", " e.g. ", " each "]
        self.bad_words += [" as-shown ", " example ", " examplary ", " exemplary ", " this ", " typically ", " known ", " large ", " least ", " one "]
        self.bad_words += [" other ", " particular ", " particularly ", " preferably ", " prior ", " art ", " related ", " said "]
        self.bad_words += [" whose ", " which ", " whether ", " above ", " above-described ", " different ", " desired "  ]
        self.bad_words += [" ("+a+") " for a in "abcdefghijklmn123456789"]


        # self.component_names = ["low pressure turbine", "high pressure turbine", "turbine blade"]

        self.component_names = ["differential pressure transducer", "fan assembly", "exhaust nozzle","fan blade", "compressor blade", "gas turbine", "turbine blade"]
        self.component_names += ["active clearance control","active combustion control","active control feedback","active pattern factor control"]
        self.component_names += ["active tip clearance control", "cowl doors","additional front fan", "additive delivery subsystem", "additively manufactured seal"]
        self.component_names += ["adjustable hanger", "air hole", "waste heat recover"]
        self.component_names += ["spool shaft", "nozzle assembly", "controller", "compressor", "turbine", "vanes", "valve", "aerofoil", "mounting", "motor","gear box", "cooler"]

        F = open(file_,"r")
        for line in F:
            line = line.replace("relation=","",1)
            line = line.split("\n")[0]
            con1, rel, con2, meta = line.split(" -->> ")
            id_, sent, org, date = meta.split("--==--")

            org = org.split("organization=")[1].replace(",","",-1)
            id_ = id_.split("patent_id=")[1]
            date = date.split("date=")[1]

            con1, name1 = self._filter(con1)
            con2, name2 = self._filter(con2)

            if con1 not in self.cmps:
                self.cmps[con1] = name1
            elif self.cmps[con1] == "":
                self.cmps[con1] = name1
            else:
                name1 = self.cmps[con1]

            if con2 not in self.cmps:
                self.cmps[con2] = name2
            elif self.cmps[con2] == "":
                self.cmps[con2] = name2
            else:
                name2 = self.cmps[con2]





            # print org, id_
            if org not in self.orgs:
                self.orgs[org] = ""

            o_name = self.orgs[org]
            if o_name in self.graphlets:
                if name1 != "" and name2 != "":
                    txt = o_name+","+date+" -->> " + name1 + " -->> contains -->> " + name2
                    if date not in self.graphlets[o_name]:
                        self.graphlets[o_name][date] = []
                    self.graphlets[o_name][date].append([txt, (con1,name1), rel, (con2,name2), meta])
                    # if name2 in ["HP turbine"]:
                    #     print o_name+","+date+" -->> " + name1 + " -->> contains -->> " + name2

        for key in self.graphlets:
            print key,"has a total of",len(self.graphlets[key]),"graphlets"

    def _read_orgs(self):
        file_ = self.save_dir+"orgs.csv"
        self.orgs = {}
        self.graphlets = {}

        if os.path.isfile(file_):
            F = open(file_,"r")
            for line in F:
                line = line.replace("\n","",1)
                line = line.replace("\r","",1)
                key,name = line.split(",")
                self.orgs[key] = name
                if name != "":
                    self.graphlets[name] = {}

    def _update_orgs(self):
        file_ = self.save_dir+"orgs.csv"
        F = open(file_,"w")
        for key in sorted(self.orgs.keys()):
            F.write(key+","+self.orgs[key]+"\r\n")
        F.close()

    def _read_components(self):
        file_ = self.save_dir+"cmps.csv"
        self.cmps = {}

        if os.path.isfile(file_):
            F = open(file_,"r")
            for line in F:
                line = line.replace("\n","",1)
                line = line.replace("\r","",1)
                key,name = line.split(",")
                self.cmps[key] = name

    def _update_components(self):
        file_ = self.save_dir+"cmps.csv"
        F = open(file_,"w")
        for key in sorted(self.cmps.keys()):
            flag = 1
            for word1 in self.bad_words:
                if word1 in " "+key+" ":
                    flag = 0
            if flag:
                F.write(key+","+self.cmps[key]+"\r\n")
        F.close()
        print len(self.cmps)

    def _build_graph(self):
        self.parts = {}
        for org in self.graphlets:
            self.parts[org] = {}
            parts = []
            for date in sorted(self.graphlets[org]):
                for graphlet in self.graphlets[org][date]:
                    # c1,rel,c2,meta =
                    if graphlet[3][1] not in parts:
                        parts.append(graphlet[3][1])
                print date,len(parts)
                self.parts[org][date] = [len(parts),sorted(parts)]

    def _draw_graph(self):
        for org in self.parts:
            prev = 0
            date_old = "0"
            if org == "GE":
                c = (1,.7,.7)
            elif org == "RR":
                c = (.7,.7,1)
            else:
                c = (.7,.7,.7)

            for date in sorted(self.parts[org]):
                parts = self.parts[org][date]
                G = nx.Graph()
                for part in parts[1]:
                    G.add_edge(org,part)
                if len(G.nodes())>prev:

                    # c = [random.random()*.5+.5]* nx.number_of_nodes(G)

                    nx.draw(G,pos=nx.spring_layout(G), node_size=500, node_color=c, vmin=0.0, vmax=1.0, with_labels=True) # use spring layout
                    plt.axis('equal')
                    # plt.show()
                    plt.savefig(self.img_dir+org+"-"+date+'.png')
                    plt.cla()
                else:
                    img = cv2.imread(self.img_dir+org+"-"+date_old+'.png')
                    cv2.imwrite(self.img_dir+org+"-"+date+'.png', img)

                prev = len(G.nodes())
                date_old = date
                G.clear()

    def _show_graph(self):
        # for org in self.parts[""]:
        dates1 = sorted(self.parts["RR"])
        dates2 = sorted(self.parts["GE"])
        dates = list(set().union(dates1,dates2))
        while(1):
            for date in sorted(dates):
                print date

                try:
                    if os.path.isfile(self.img_dir+"RR"+"-"+date+'.png'):
                        img1 = cv2.imread(self.img_dir+"RR"+"-"+date+'.png')
                        img1 = cv2.resize(img1, (600, 500))

                    if os.path.isfile(self.img_dir+"GE"+"-"+date+'.png'):
                        img2 = cv2.imread(self.img_dir+"GE"+"-"+date+'.png')
                        img2 = cv2.resize(img2, (600, 500))


                    vis = np.concatenate((img1, img2), axis=1)
                    cv2.putText(vis, "Date:"+date[4:6]+"-"+date[:4], (490, 490), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (50,50,250), lineType=cv2.LINE_AA)
                    cv2.imshow("vis",vis)
                    cv2.waitKey(100)
                except:
                    pass




def main():
    R = read_graphlets()

    # R._update_graphlets()
    R._read_orgs()
    R._read_components()

    R._read_graphlets()

    R._update_orgs()
    R._update_components()

    R._build_graph()
    # R._draw_graph()
    R._show_graph()



if __name__=="__main__":
    main()
